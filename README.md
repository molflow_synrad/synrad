# SynRad+
A Monte Carlo simulator for Synchrotron Radiation

**Authors:** Roberto KERSEVAN, Marton ADY, Jean-Luc PONS  
**Website:** https://cern.ch/molflow  
**Copyright:** CERN (2025)  
**License:** GNU GPLv2 or later

# Cloning the project
`git clone --recurse-submodules https://gitlab.cern.ch/molflow_synrad/synrad.git`

# Versions

This is the legacy (SynRad 1.4) repository.
For the latest 1.5 betas, check out...

* `master_cli` branch of the main (`synrad`) repo
* `master_synradcli` branch of the shared (`src_shared`) repo
  
# Building

For the full readme, including building, please refer to the [MolFlow repo](https://gitlab.cern.ch/molflow_synrad/molflow)