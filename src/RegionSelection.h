
#ifndef _RegionSelectionH_
#define _RegionSelectionH_

#include "GLApp/GLWindow.h"
#include "GLApp/GLList.h"
#include "GLApp/GLButton.h"
#include "GLApp/GLToggle.h"
#include "GLApp/GLTitledPanel.h"
#include "GLApp/GLCombo.h"
#include "Worker.h"
#include "Random.h"
#include <set>

#define NB_RPCOLUMN 3

class Facet;

class RegionSelection : public GLWindow {

public:

  // Construction
  RegionSelection(Worker *worker);

  // Component method
  void Display(Worker *w,int regionId);
  void Update();
  void UpdateSelection(bool externalChange);

  // Implementation
  void ProcessMessage(GLComponent *src,int message);
  void SetBounds(int x,int y,int w,int h);
  void SelectPoint(int idx);
  size_t GetRegionId();

private:

  char *GetCountStr(Facet *f);
  void UpdateTable();
  char *FormatCell(int idx, int mode, Region_full *reg);
  void PlaceComponents();

  size_t displayedRegion;
    Worker      *worker;
    std::shared_ptr<std::set<size_t>> selectedRegions;
    std::vector<size_t> tmpSelectedRegions;
    GLList      *regionList;


  GLTitledPanel *sPanel;          // Toggle panel
  GLToggle      *show[NB_RPCOLUMN-1];
  int            shown[NB_RPCOLUMN];

  GLButton    *checkAllButton;
  GLButton    *uncheckAllButton;
  GLButton    *dismissButton;
  GLButton	  *updateButton;
  GLButton	  *updateSelButton;
    GLButton	  *selectButton;
    GLButton	  *deselectButton;
    GLTextField	  *selectionText;
};

#endif /* _RegionSelectionH_ */
