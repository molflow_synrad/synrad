//
// Created by pbahr on 12/10/2020.
//

#include <Helper/StringHelper.h>
#include <Helper/MathTools.h>
#include <numeric>
#include "FormulaEvaluator_SR.h"
#include "Worker.h"
#include "SynradGeometry.h"
#include "Facet_shared.h"

FormulaEvaluator_SR::FormulaEvaluator_SR(Worker* w, SynradGeometry* geom, std::vector<SelectionGroup>* sel){
    worker = w;
    geometry = geom;
    selections = sel;
}

bool FormulaEvaluator_SR::EvaluateVariable(VLIST *v) {
    bool ok = true;
    Geometry* geom = worker->GetGeometry();
    size_t nbFacet = geom->GetNbFacet();
    int idx;

    if ((idx = GetVariable(v->name, "A")) > 0) {
        ok = (idx <= nbFacet);
        if (ok) v->value = geom->GetFacet(idx - 1)->facetHitCache.hit.nbAbsEquiv;
    }
    else if ((idx = GetVariable(v->name, "MCH")) > 0) {
        ok = (idx > 0 && idx <= nbFacet);
        if (ok) v->value = (double)geom->GetFacet(idx - 1)->facetHitCache.hit.nbMCHit;
    }
    else if ((idx = GetVariable(v->name, "H")) > 0) {
        ok = (idx <= nbFacet);
        if (ok) v->value = (double)geom->GetFacet(idx - 1)->facetHitCache.hit.nbHitEquiv;
    }
    else if ((idx = GetVariable(v->name, "F")) > 0) {
        ok = (idx <= nbFacet);
        if (ok) v->value = (double)geom->GetFacet(idx - 1)->facetHitCache.hit.fluxAbs / worker->no_scans;
    }
    else if ((idx = GetVariable(v->name, "P")) > 0) {
        ok = (idx <= nbFacet);
        if (ok) v->value = (double)geom->GetFacet(idx - 1)->facetHitCache.hit.powerAbs / worker->no_scans;
    }
    else if ((idx = GetVariable(v->name, "AR")) > 0) {
        ok = (idx <= nbFacet);
        if (ok) v->value = geom->GetFacet(idx - 1)->sh.area;
    }
    else if (iequals(v->name, "SCANS")) {
        v->value = worker->no_scans;
    }
    else if (iequals(v->name, "SUMDES")) {
        v->value = (double)worker->globalHitCache.globalHits.hit.nbDesorbed;
    }
    else if (iequals(v->name, "SUMABS")) {
        v->value = worker->globalHitCache.globalHits.hit.nbAbsEquiv;
    }
    else if (iequals(v->name, "SUMHIT")) {
        v->value = (double)worker->globalHitCache.globalHits.hit.nbMCHit;
    }
    else if (iequals(v->name, "SUMFLUX")) {
        v->value = worker->globalHitCache.globalHits.hit.fluxAbs / worker->no_scans;
    }
    else if (iequals(v->name, "SUMPOWER")) {
        v->value = worker->globalHitCache.globalHits.hit.powerAbs / worker->no_scans;
    }
    else if (iequals(v->name, "MPP")) {
        v->value = worker->globalHitCache.distTraveledTotal / (double)worker->globalHitCache.globalHits.hit.nbDesorbed;
    }
    else if (iequals(v->name, "MFP")) {
        v->value = worker->globalHitCache.distTraveledTotal / worker->globalHitCache.globalHits.hit.nbHitEquiv;
    }
    else if (iequals(v->name, "ABSAR")) {
        double sumArea = 0.0;
        for (int i = 0; i < geom->GetNbFacet(); i++) {
            Facet *f = geom->GetFacet(i);
            if (f->sh.sticking > 0.0) sumArea += f->sh.area*f->sh.opacity*(f->sh.is2sided ? 2.0 : 1.0);
        }
        v->value = sumArea;
    }
    else if (iequals(v->name, "KB")) {
        v->value = 1.3806504e-23;
    }
    else if (iequals(v->name, "R")) {
        v->value = 8.314472;
    }
    else if (iequals(v->name, "Na")) {
        v->value = 6.02214179e23;
    }
    else if ((beginsWith(v->name, "SUM(") || beginsWith(v->name, "sum(")) /*|| (beginsWith(v->name, "AVG("))*/ && endsWith(v->name, ")")) {
        //bool avgMode = beginsWith(v->name, "AVG("); //else SUM mode
        std::string inside = v->name; inside.erase(0, 4); inside.erase(inside.size() - 1, 1);
        std::vector<std::string> tokens = SplitString(inside, ',');
        if (!Contains({ 2,3 }, tokens.size()))
            return false;
            /*if (avgMode) {
                if (!Contains({ "P","DEN","Z" }, tokens[0]))
                    return false;
            }*/
        else {
            if (!Contains({ "H","MCH","A","F","P","AR","h","mch","a","f","p","ar" }, tokens[0]))
                return false;
        }
        std::vector<size_t> facetsToSum;
        if (tokens.size() == 3) { // Like SUM(H,3,6) = H3 + H4 + H5 + H6
            size_t startId, endId, pos;
            try {
                startId = std::stol(tokens[1], &pos); if (pos != tokens[1].size() || startId > geom->GetNbFacet() || startId == 0) return false;
                endId = std::stol(tokens[2], &pos); if (pos != tokens[2].size() || endId > geom->GetNbFacet() || endId == 0) return false;
            }
            catch (...) {
                return false;
            }
            if (!(startId < endId)) return false;
            facetsToSum = std::vector<size_t>(endId - startId + 1);
            std::iota(facetsToSum.begin(), facetsToSum.end(), startId - 1);
        }
        else { //Selection group
            if (!(beginsWith(tokens[1], "S") || beginsWith(tokens[1], "s"))) return false;
            std::string selIdString = tokens[1]; selIdString.erase(0, 1);
            if (Contains({ "EL","el" }, selIdString)) { //Current selections
                facetsToSum = geom->GetSelectedFacets();
            }
            else {
                size_t selGroupId, pos;
                try {
                    selGroupId = std::stol(selIdString, &pos); if (pos != selIdString.size() || selGroupId > selections->size() || selGroupId == 0) return false;
                }
                catch (...) {
                    return false;
                }
                facetsToSum = (*selections)[selGroupId - 1].selection;
            }
        }
        size_t sumLL = 0;
        double sumD = 0.0;
        double sumArea = 0.0; //We average by area
        for (auto sel : facetsToSum) {
            if (Contains({ "MCH", "mch" }, tokens[0])) {
                sumLL += geom->GetFacet(sel)->facetHitCache.hit.nbMCHit;
            }
            else if (Contains({ "H", "h" }, tokens[0])) {
                sumD += geom->GetFacet(sel)->facetHitCache.hit.nbHitEquiv;
            }
            else if (Contains({ "A","a" }, tokens[0])) {
                sumD += geom->GetFacet(sel)->facetHitCache.hit.nbAbsEquiv;
            }
            else if (Contains({ "AR","ar" }, tokens[0])) {
                sumArea += geom->GetFacet(sel)->GetArea();
            }
            else if (Contains({ "F","f" }, tokens[0])) {
                sumD += geom->GetFacet(sel)->facetHitCache.hit.fluxAbs /*/ worker->no_scans*/;
            }
            else if (Contains({ "P","p" }, tokens[0])) {
                sumD += geom->GetFacet(sel)->facetHitCache.hit.powerAbs/* / worker->no_scans*/;
            }
            else return false;
        }
        //if (avgMode) v->value = sumD * worker->GetMoleculesPerTP(worker->displayedMoment)*1E4 / sumArea;
        /*else*/ if (Contains({ "AR" , "ar"},tokens[0])) v->value = sumArea;
        else if (Contains({ "H", "h", "A", "a" }, tokens[0])) v->value = sumD;
        else if (Contains({ "F","P","f","p" }, tokens[0])) v->value = sumD / worker->no_scans;
        else v->value = (double)sumLL;
    }
    else ok = false;
    return ok;
}