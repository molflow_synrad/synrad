
#ifndef _REGION_FULL_
#define _REGION_FULL_

#include <stdio.h>
#include <fstream>
#include "Region_mathonly.h"
#if defined(WIN32) || defined(_WIN32) || defined(WIN64) || defined(_WIN64)
#include <process.h> //for getpid
#endif

#include "File.h"

#include "GLApp/GLToolkit.h"
#include "GLApp/GLProgress.h"
//#include "GLApp\GLTypes.h"
#include <cereal/types/string.hpp>
#include <cereal/types/vector.hpp>
#include <cereal/types/utility.hpp>

class Region_full : public Region_mathonly { //Beam trajectory
public:
	
	//References
	std::string fileName,MAGXfileName,MAGYfileName,MAGZfileName,BXYfileName;
	bool isLoaded;
	Vector3d AABBmin,AABBmax;
	int selectedPointId; //can be -1 if nothing's selected

	//Methods
	Region_full();
	~Region_full();
	//Region_full(const Region_full &src); //used at push_back
	//Region_full& operator=(const Region_full &src); //used in CopyGeometryBuffer
	//need converter between full<->mathonly?
	
	void CalculateTrajectory(int max_steps);
	Trajectory_Point OneStep(int pointId); //moves the beam by dL length from the i-th calculated point
	void CalcPointProperties(int pointId); //calculates magnetic field, sigma values, etc. for a trajectory point
	bool isOutsideBoundaries(Vector3d a,bool recalcDirs);
	void LoadPAR(FileReader *file);
	void LoadParam(FileReader *f);
	Distribution2D LoadMAGFile(FileReader *file,Vector3d *dir,double *period,double *phase,int mode);
	void PrecalculateQuadParams(); //Precalculate reused mathematical formulas once the MAG file was read
	int LoadBXY(const std::string& fileName); //Throws error
	void Render(const size_t& regionId, const size_t& dispNumTraj, GLMATERIAL *B_material, const double& vectorLength);
	void SelectTrajPoint(int x,int y, size_t regionId);
	void SaveParam(FileWriter *f);

    /*template<class Archive>
    void serialize(Archive & archive)
    {

        archive(
                CEREAL_NVP(fileName),
                CEREAL_NVP(MAGXfileName),CEREAL_NVP(MAGYfileName),CEREAL_NVP(MAGZfileName),
                CEREAL_NVP(BXYfileName),
                CEREAL_NVP(isLoaded),
                CEREAL_NVP(AABBmin),
                CEREAL_NVP(AABBmax),
                CEREAL_NVP(selectedPointId),

                CEREAL_NVP(params),
                CEREAL_NVP(Points),
                CEREAL_NVP(Bx_distr),CEREAL_NVP(By_distr),CEREAL_NVP(Bx_distr),
                CEREAL_NVP(latticeFunctions)
        );
    }*/
};

#endif