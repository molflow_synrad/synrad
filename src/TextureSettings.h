
#include "GLApp/GLWindow.h"
#include "GLApp/GLButton.h"
#include "GLApp/GLTextField.h"
#include "GLApp/GLLabel.h"
#include "GLApp/GLToggle.h"
#include "GLApp/GLTitledPanel.h"
#include "GLApp/GLGradient.h"
#include "GeometryViewer.h"

#ifndef _TEXTURESETTINGSH_
#define _TEXTURESETTINGSH_

class SynradGeometry;

class TextureSettings : public GLWindow {

public:

  // Construction
  TextureSettings();

  // Component methods
  void Display(Worker *w,GeometryViewer **v);
  void Update();

  // Implementation
  void ProcessMessage(GLComponent *src,int message);

private:

  void UpdateSize();

  Worker         *worker;
  SynradGeometry *geom;
  GeometryViewer **viewers;

  GLToggle      *texAutoScale;
  GLTextField   *texMinText;
  GLTextField   *texMaxText;
  GLLabel       *texCMinText;
  GLLabel       *texCMaxText;
  GLToggle      *colormapBtn;
  GLTextField   *swapText;
  GLGradient    *gradient;
  GLToggle      *logBtn;
  GLButton      *setCurrentButton;
  GLCombo       *modeCombo;

  GLButton    *updateButton;

};

#endif /* _TEXTURESETTINGSH_ */
