
#pragma once

#include "GLApp/GLWindow.h"
#include "GLApp/GLChart/GLChart.h"
#include "GLApp/GLLabel.h"
#include "GLApp/GLCombo.h"
#include "GLApp/GLButton.h"
#include "GLApp/GLParser.h"
#include "GLApp/GLTextField.h"
#include "Worker.h"
#include "Geometry_shared.h"

class SpectrumPlotter : public GLWindow {

public:

  // Construction
  SpectrumPlotter();

  // Component method
  void Display(Worker *w);
  void SetScale();
  void Refresh();
  void Update(float appTime,bool force=false);
  void Reset();

  // Implementation
  void ProcessMessage(GLComponent *src,int message);
  void SetBounds(int x,int y,int w,int h);

private:

  void addView(int facet,int mode);
  void remView(int facet,int mode);
  void refreshViews();

  Worker      *worker;
  GLButton    *dismissButton;
  GLChart     *chart;
  GLCombo     *specCombo;
  GLToggle    *logToggle;
  GLToggle    *normToggle;
  GLButton    *selButton;
  GLButton    *addButton;
  GLButton    *removeButton;
  GLButton    *resetButton;

  GLDataView  *views[32];
  GLColor    *colors[8];

  int          nbColors;
  int          nbView;
  double       delta;
  float        lastUpdate;

};