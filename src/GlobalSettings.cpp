
#include <sstream>
#include "GLApp/GLLabel.h"
#include "GlobalSettings.h"
#include "GLApp/GLToolkit.h"
#include "GLApp/GLMessageBox.h"
#include "GLApp/GLInputBox.h"
#include "Helper/MathTools.h"
#include "SynRad.h"
#include "AppUpdater.h"
#include "SMP.h"
#include "ProcessControl.h"

extern SynRad *mApp;

static const int   plWidth[] = { 60,40,70,70,335 };
static const char *plName[] = { "#","PID","Mem Usage","Mem Peak",/*"CPU",*/"Status" };
static const int   plAligns[] = { ALIGN_LEFT,ALIGN_LEFT,ALIGN_LEFT,ALIGN_LEFT,ALIGN_LEFT };

//HANDLE synradHandle;

GlobalSettings::GlobalSettings(Worker *w) : GLWindow() {

    worker = w;

	int wD = 610;
	int hD = 500;

	const int checkboxHeight = 25;

	SetTitle("Global Settings");
	SetIconfiable(true);

	GLTitledPanel* viewSettingsPanel = new GLTitledPanel("View settings");
	viewSettingsPanel->SetBounds(5, 2, 300, 100);
	Add(viewSettingsPanel);

	chkAntiAliasing = new GLToggle(0, "Anti-Aliasing");
	viewSettingsPanel->SetCompBounds(chkAntiAliasing, 10, 20, 80, 19);
	viewSettingsPanel->Add(chkAntiAliasing);

	chkWhiteBg = new GLToggle(0, "White Background");
	viewSettingsPanel->SetCompBounds(chkWhiteBg, 10, 50, 80, 19);
	viewSettingsPanel->Add(chkWhiteBg);

	leftHandedToggle = new GLToggle(0, "Left-handed coord. system");
	viewSettingsPanel->SetCompBounds(leftHandedToggle, 140, 20, 80, 19);
	viewSettingsPanel->Add(leftHandedToggle);

	highlightNonplanarToggle = new GLToggle(0, "Highlight non-planar facets");
	viewSettingsPanel->SetCompBounds(highlightNonplanarToggle, 140, 50, 80, 19);
	viewSettingsPanel->Add(highlightNonplanarToggle);

	highlightSelectionToggle = new GLToggle(0, "Highlight selected facets");
	viewSettingsPanel->SetCompBounds(highlightSelectionToggle, 140, 80, 80, 19);
	viewSettingsPanel->Add(highlightSelectionToggle);

	GLTitledPanel* simuSettingsPanel = new GLTitledPanel("Simulation settings");
	simuSettingsPanel->SetBounds(310, 2, 295, 100);
	Add(simuSettingsPanel);

	lowFluxToggle = new GLToggle(0, "Enable low flux mode");
	simuSettingsPanel->SetCompBounds(lowFluxToggle, 10, 20, 150, 19);
	simuSettingsPanel->Add(lowFluxToggle);

	lowFluxInfo = new GLButton(0, "Info");
	simuSettingsPanel->SetCompBounds(lowFluxInfo, 160, 15, 40, 19);
	simuSettingsPanel->Add(lowFluxInfo);

	GLLabel* cutoffLabel = new GLLabel("Cutoff ratio:");
	simuSettingsPanel->SetCompBounds(cutoffLabel, 30, 50, 80, 19);
	simuSettingsPanel->Add(cutoffLabel);

	cutoffText = new GLTextField(0, "");
	simuSettingsPanel->SetCompBounds(cutoffText, 120, 50, 80, 19);
	cutoffText->SetEditable(false);
	simuSettingsPanel->Add(cutoffText);

	chkNewReflectionModel = new GLToggle(0, "Use new reflection model");
	simuSettingsPanel->SetCompBounds(chkNewReflectionModel, 10, 80, 160, 19);
	simuSettingsPanel->Add(chkNewReflectionModel);

	newReflectmodeInfo = new GLButton(0, "Info");
	simuSettingsPanel->SetCompBounds(newReflectmodeInfo, 160, 75, 40, 19);
	simuSettingsPanel->Add(newReflectmodeInfo);

	GLTitledPanel* programSettingsPanel = new GLTitledPanel("Program settings");
	programSettingsPanel->SetBounds(5, 110, 600, 100);
	Add(programSettingsPanel);

	GLLabel* asLabel = new GLLabel("Autosave frequency (minutes):");
	programSettingsPanel->SetCompBounds(asLabel, 10, 20, 160, 19);
	programSettingsPanel->Add(asLabel);

	autoSaveText = new GLTextField(0, "");
	programSettingsPanel->SetCompBounds(autoSaveText, 160, 20, 30, 19);
	programSettingsPanel->Add(autoSaveText);

	chkSimuOnly = new GLToggle(0, "Autosave only when simulation is running");
	programSettingsPanel->SetCompBounds(chkSimuOnly, 10, 50, 160, 19);
	programSettingsPanel->Add(chkSimuOnly);

	chkCheckForUpdates = new GLToggle(0, "Check for updates at startup");
	programSettingsPanel->SetCompBounds(chkCheckForUpdates, 315, 20, 100, 19);
	//chkCheckForUpdates->SetEnabled(false);
	programSettingsPanel->Add(chkCheckForUpdates);

	chkAutoUpdateFormulas = new GLToggle(0, "Auto update formulas");
	programSettingsPanel->SetCompBounds(chkAutoUpdateFormulas, 315, 50, 160, 19);
	programSettingsPanel->Add(chkAutoUpdateFormulas);

	chkCompressSavedFiles = new GLToggle(0, "Compress saved files (use .SYN7Z format)");
	programSettingsPanel->SetCompBounds(chkCompressSavedFiles, 10, 80, 100, 19);
	programSettingsPanel->Add(chkCompressSavedFiles);

	applyButton = new GLButton(0, "Apply above settings");
	programSettingsPanel->SetCompBounds(applyButton, wD / 2 - 65, 105, 130, 19);
	Add(applyButton); //Would be out of panel

	const int procControlPosY = 250;
	const int procControlHeight = 225;
	auto* threadControlPanel = new GLTitledPanel("Thread control");
	threadControlPanel->SetBounds(5, procControlPosY, wD - 10, procControlHeight);
	Add(threadControlPanel);

	processList = new GLList(0);
	processList->SetHScrollVisible(true);
	processList->SetSize(5, MAX_PROCESS + 1);
	processList->SetColumnWidths((int*)plWidth);
	processList->SetColumnLabels((const char**)plName);
	processList->SetColumnAligns((int*)plAligns);
	processList->SetColumnLabelVisible(true);
	threadControlPanel->SetCompBounds(processList, 10, 20, wD - 25, procControlHeight - 70);
	threadControlPanel->Add(processList);
	
	char tmp[128];
	sprintf(tmp, "Number of CPU cores:     %zd", mApp->numCPU);
	auto* coreLabel = new GLLabel(tmp);
	threadControlPanel->SetCompBounds(coreLabel, 10, procControlHeight - 45, 120, 19);
	threadControlPanel->Add(coreLabel);

	auto* l1 = new GLLabel("Number of threads:");
	threadControlPanel->SetCompBounds(l1, 10, procControlHeight - 25, 120, 19);
	threadControlPanel->Add(l1);

	nbProcText = new GLTextField(0, "");
	nbProcText->SetEditable(true);
	threadControlPanel->SetCompBounds(nbProcText, 135, procControlHeight - 25, 30, 19);
	threadControlPanel->Add(nbProcText);

	restartButton = new GLButton(0, "Apply and restart threads");
	threadControlPanel->SetCompBounds(restartButton, 170, procControlHeight - 25, 150, 19);
	threadControlPanel->Add(restartButton);

	maxButton = new GLButton(0, "Change MAX generated photons");
	threadControlPanel->SetCompBounds(maxButton, wD - 195, procControlHeight - 25, 180, 19);
	threadControlPanel->Add(maxButton);

	// Center dialog
	int wS,hS;
	GLToolkit::GetScreenSize(&wS,&hS);
	int xD = (wS-wD)/2;
	int yD = (hS-hD)/2;
	SetBounds(xD,yD,wD,hD);

	RestoreDeviceObjects();

	lastUpdate = 0;
	//for(int i=0;i<MAX_PROCESS;i++) lastCPUTime[i]=-1.0f;
	//memset(lastCPULoad,0,MAX_PROCESS*sizeof(float));
}

void GlobalSettings::Display(Worker *w) {
	worker = w;
	Update();	
	SetVisible(true);
}

void GlobalSettings::SMPUpdate() {

	int time = SDL_GetTicks();

	if (!IsVisible() || IsIconic()) return;
	size_t nb = worker->GetProcNumber();
	if (processList->GetNbRow() != (nb + 1)) processList->SetSize(5, nb + 1, true);

	if (time - lastUpdate>333) {

        char tmp[512];
        size_t  states[MAX_PROCESS];
        std::vector<std::string> statusStrings(MAX_PROCESS);

        memset(states, 0, MAX_PROCESS * sizeof(int));
        worker->GetProcStatus(states, statusStrings);

        std::vector<SubProcInfo> procInfo;
        worker->GetProcStatus(procInfo);

	processList->ResetValues();

	//Interface
#if defined(WIN32) || defined(_WIN32) || defined(WIN64) || defined(_WIN64)
        size_t currPid = GetCurrentProcessId();
    PROCESS_INFO parentInfo;
    GetProcInfo(currPid, &parentInfo);

    processList->SetValueAt(0, 0, "Interface");
	sprintf(tmp, "%zd", currPid);
	processList->SetValueAt(1, 0, tmp, currPid);
	sprintf(tmp, "%.0f MB", (double)parentInfo.mem_use / (1024.0*1024.0));
	processList->SetValueAt(2, 0, tmp);
	sprintf(tmp, "%.0f MB", (double)parentInfo.mem_peak / (1024.0*1024.0));
	processList->SetValueAt(3, 0, tmp);

#else
        size_t currPid = getpid();
        PROCESS_INFO parentInfo;
        GetProcInfo(currPid, &parentInfo);

    //GetProcInfo(currpid, &pInfo);
    processList->SetValueAt(0, 0, "Interface");
    sprintf(tmp, "%zd", currPid);
    processList->SetValueAt(1, 0, tmp, (int)currPid);

    sprintf(tmp, "%.0f MB", (double)parentInfo.mem_use / (1024.0));
    processList->SetValueAt(2, 0, tmp);
    sprintf(tmp, "%.0f MB", (double)parentInfo.mem_peak / (1024.0));
    processList->SetValueAt(3, 0, tmp);
    //sprintf(tmp, "%d %%", (int)pInfo.cpu_time);
    //processList->SetValueAt(4, 0, tmp);
#endif

        size_t i = 1;
        for (auto& proc : procInfo) {
            DWORD pid = proc.procId;
            sprintf(tmp, "Subproc.%lu", i);
            processList->SetValueAt(0, i, tmp);
            sprintf(tmp, "%d", pid);
            processList->SetValueAt(1, i, tmp);

#if defined(WIN32) || defined(_WIN32) || defined(WIN64) || defined(_WIN64)
            PROCESS_INFO pInfo;
        if (!GetProcInfo(pid, &pInfo)) {
			processList->SetValueAt(2, i, "0 KB");
			processList->SetValueAt(3, i, "0 KB");
			//processList->SetValueAt(4,i,"0 %");
			processList->SetValueAt(4, i, "Dead");
		}
		else {
			sprintf(tmp, "%.0f MB", (double)pInfo.mem_use / (1024.0*1024.0));
			processList->SetValueAt(2, i, tmp);
			sprintf(tmp, "%.0f MB", (double)pInfo.mem_peak / (1024.0*1024.0));
			processList->SetValueAt(3, i, tmp);

			// State/Status
			std::stringstream tmp; tmp << "[" << prStates[states[i-1]] << "] " << statusStrings[i-1];
			processList->SetValueAt(4, i, tmp.str().c_str());
		}


#else
            if (pid == currPid) { // TODO: Check if this is wanted
                processList->SetValueAt(2, i, "0 KB");
                processList->SetValueAt(3, i, "0 KB");
                //processList->SetValueAt(4,i,"0 %");
                processList->SetValueAt(4, i, "Dead");
            }
            else {
                PROCESS_INFO pInfo = proc.runtimeInfo;
                //GetProcInfo(pid, &pInfo);


                sprintf(tmp, "%.0f MB", (double)pInfo.mem_use / (1024.0));
                processList->SetValueAt(2, i, tmp);
                sprintf(tmp, "%.0f MB", (double)pInfo.mem_peak / (1024.0));
                processList->SetValueAt(3, i, tmp);
                //sprintf(tmp, "%d %%", (int)pInfo.cpu_time);
                //processList->SetValueAt(4, i, tmp);

                // State/Status
                std::stringstream tmp_ss; tmp_ss << "[" << prStates[states[i-1]] << "] " << statusStrings[i-1];
                processList->SetValueAt(4, i, tmp_ss.str().c_str());
            }
#endif
            ++i;
        }
        lastUpdate = SDL_GetTicks();
	}

}

/**
* \brief Function to apply changes to the number of processes.
*/
void GlobalSettings::RestartProc() {

	int nbProc;
	if (!nbProcText->GetNumberInt(&nbProc)) {
		GLMessageBox::Display("Invalid process number", "Error", GLDLG_OK, GLDLG_ICONERROR);
	}
	else {
		if( mApp->AskToReset() ) {
			if(nbProc<=0 || nbProc>MAX_PROCESS) {
				GLMessageBox::Display("Invalid process number [1..32]","Error",GLDLG_OK,GLDLG_ICONERROR);
			} else {
				try {
					worker->Stop_Public();
					worker->SetProcNumber(nbProc);
					worker->RealReload(true);
					mApp->SaveConfig();
				} catch(Error &e) {
					GLMessageBox::Display((char *)e.what(),"Error",GLDLG_OK,GLDLG_ICONERROR);
				}
			}
		}
	}

}

/**
* \brief Function for processing various inputs (button, check boxes etc.)
* \param src Exact source of the call
* \param message Type of the source (button)
*/
void GlobalSettings::ProcessMessage(GLComponent *src, int message) {

	switch(message) {
	case MSG_BUTTON:

		if(src==cancelButton) {

			GLWindow::ProcessMessage(NULL,MSG_CLOSE);

		} else if (src==restartButton) {
			RestartProc();
		}
		else if (src==maxButton) {
			if( worker->GetGeometry()->IsLoaded() ) {
				char tmp[128];
				sprintf(tmp,"%zd",worker->ontheflyParams.desorptionLimit);
				char *val = GLInputBox::GetInput(tmp,"Desorption max (0=>endless)","Edit MAX");
                if (val) {
                    char* endptr;
                    size_t maxDes = strtold(val,&endptr); // use double function to allow exponential format
                    if (val==endptr) {
                        GLMessageBox::Display("Invalid 'maximum desorption' number", "Error", GLDLG_OK, GLDLG_ICONERROR);
                    }
                    else {
                        worker->ontheflyParams.desorptionLimit = maxDes;
                        worker->ChangeSimuParams(); //Sync with subprocesses
                    }
                }
			} else {
				GLMessageBox::Display("No geometry loaded.","No geometry",GLDLG_OK,GLDLG_ICONERROR);
			}
		} else if (src==applyButton) {
			mApp->antiAliasing = chkAntiAliasing->GetState();
			mApp->whiteBg = chkWhiteBg->GetState();
            mApp->highlightSelection = highlightSelectionToggle->GetState();
            if(mApp->highlightSelection)
                worker->GetGeometry()->UpdateSelection();
			mApp->highlightNonplanarFacets = highlightNonplanarToggle->GetState();
            mApp->leftHandedView = (bool)leftHandedToggle->GetState();

			for (auto & i : mApp->viewer)
				i->UpdateMatrix();

			bool updateCheckPreference = chkCheckForUpdates->GetState();
			if (mApp->appUpdater) {
				if (mApp->appUpdater->IsUpdateCheckAllowed() != updateCheckPreference) {
					mApp->appUpdater->SetUserUpdatePreference(updateCheckPreference);
				}
			}
			
			mApp->autoUpdateFormulas = chkAutoUpdateFormulas->GetState();
			mApp->compressSavedFiles = chkCompressSavedFiles->GetState();
			mApp->autoSaveSimuOnly = chkSimuOnly->GetState();
			if (worker->wp.newReflectionModel != (chkNewReflectionModel->GetState()==1)) {
				if (mApp->AskToReset()) {
					worker->wp.newReflectionModel = (chkNewReflectionModel->GetState() == 1);
					mApp->changedSinceSave = true;
					worker->Reload();
					mApp->PlaceScatteringControls(worker->wp.newReflectionModel);
				}
				else { //User cancelled, revert checkbox state
					chkNewReflectionModel->SetState(worker->wp.newReflectionModel);
				}
			}


			double cutoffnumber;
			if (!cutoffText->GetNumber(&cutoffnumber) || !(cutoffnumber>0.0 && cutoffnumber<1.0)) {
				GLMessageBox::Display("Invalid cutoff ratio, must be between 0 and 1", "Error", GLDLG_OK, GLDLG_ICONWARNING);
				return;
			}

			if (!IsEqual(worker->ontheflyParams.lowFluxCutoff, cutoffnumber) || (int)worker->ontheflyParams.lowFluxMode != lowFluxToggle->GetState()) {
				worker->ontheflyParams.lowFluxCutoff = cutoffnumber;
				worker->ontheflyParams.lowFluxMode = lowFluxToggle->GetState();
				worker->ChangeSimuParams();
			}

			double autosavefreq;
			if (!autoSaveText->GetNumber(&autosavefreq) || !(autosavefreq > 0.0)) {
				GLMessageBox::Display("Invalid autosave frequency", "Error", GLDLG_OK, GLDLG_ICONERROR);
				return;
			}
			mApp->autoSaveFrequency = autosavefreq;

			//GLWindow::ProcessMessage(NULL,MSG_CLOSE); 
			return;
		}
		else if (src == lowFluxInfo) {
			GLMessageBox::Display("Low flux mode helps to gain more statistics on low flux/power parts of the system, at the expense\n"
				"of higher flux/power parts. If a traced photon reflects from a low reflection probability surface, regardless of that probability,\n"
				"a reflected test photon representing a reduced flux will still be traced. Therefore test photons can reach low flux areas more easily, but\n"
				"at the same time tracing a test photon takes longer. The cutoff ratio defines what ratio of the originally generated flux/power\n"
				"can be neglected. If, for example, it is 0.001, then, when after subsequent reflections the test photon carries less than 0.1%\n"
				"of the original flux, it will be eliminated. A good advice is that if you'd like to see flux across N orders of magnitude, set it to 1E-N"
				, "Low flux mode", GLDLG_OK, GLDLG_ICONINFO);
			return;
		} else if (src == newReflectmodeInfo) {
			GLMessageBox::Display("In the old reflection model, both the material reflection probability and the new, reflected photon direction\n"
				"depend on the ""roughness ratio"" parameter, which is generally the ratio of the RMS roughness and the autocorrelation length\n"
				"of the rough surface.\n"
				"In contrast, the new reflection model, described by Dugan and Sagan in the manual of Synrad3D (Cornell), treats the reflection\n"
				"probability as the function of photon energy and grazing angle only, and then perturbates the reflection direction with an offset.\n"
				"It also has two input paramters (roughness and autocorr. length) for a surface, and calculates the Debye-Waller factor (the probability\n"
				"of specular reflection).\n"
				"Until this new model is succesfully compared with Synrad3D and real life, using the old model is recommended."
				, "New reflection model", GLDLG_OK, GLDLG_ICONINFO);
			return;
		}
		break;

	case MSG_TEXT:
		ProcessMessage(applyButton, MSG_BUTTON);
		break;
	

	case MSG_TOGGLE:
		if (src == lowFluxToggle) {
			cutoffText->SetEditable(lowFluxToggle->GetState());
		}
		break;
	}
	GLWindow::ProcessMessage(src, message);
}

/**
* \brief Function to change global settings values (may happen due to change or load of file etc.)
*/
void GlobalSettings::Update() {
    char tmp[256];
    chkAntiAliasing->SetState(mApp->antiAliasing);
    chkWhiteBg->SetState(mApp->whiteBg);
    highlightSelectionToggle->SetState(mApp->highlightSelection);
    leftHandedToggle->SetState(mApp->leftHandedView);

    cutoffText->SetText(worker->ontheflyParams.lowFluxCutoff);
    cutoffText->SetEditable(worker->ontheflyParams.lowFluxMode);
    lowFluxToggle->SetState(worker->ontheflyParams.lowFluxMode);
	highlightNonplanarToggle->SetState(mApp->highlightNonplanarFacets);
    chkNewReflectionModel->SetState(worker->wp.newReflectionModel);

    sprintf(tmp,"%g",mApp->autoSaveFrequency);
    autoSaveText->SetText(tmp);
    chkSimuOnly->SetState(mApp->autoSaveSimuOnly);
    if (mApp->appUpdater) { //Updater initialized
        chkCheckForUpdates->SetState(mApp->appUpdater->IsUpdateCheckAllowed());
    }
    else {
        chkCheckForUpdates->SetState(0);
        chkCheckForUpdates->SetEnabled(false);
    }
    chkAutoUpdateFormulas->SetState(mApp->autoUpdateFormulas);
    chkCompressSavedFiles->SetState(mApp->compressSavedFiles);

    size_t nb = worker->GetProcNumber();
    sprintf(tmp, "%zd", nb);
    nbProcText->SetText(tmp);
}
