

#pragma once 

#include "Interface.h"
//#include "ExportDesorption.h"
#include "FacetMesh.h"
#include "FacetDetails.h"
#include "TrajectoryDetails.h"
#include "Viewer3DSettings.h"
#include "TextureSettings.h"
#include "GlobalSettings.h"

class ProfilePlotter;
class SpectrumPlotter;
class TexturePlotter;
class RegionInfo;
class RegionEditor;
class RegionSelection;

class Worker;

class SynRad : public Interface
{
public:
    SynRad();

    void LoadFile(std::string fileName="");
	void InsertGeometry(bool newStr,std::string fileName="");
	void SaveFile();

    // Util functions
	//void SendHeartBeat(bool forced=false);
    //void SetParam(GLTextField *txt,double value);
    void PlaceComponents();
	void LoadParam(char *fName=NULL,int position=-1);
	//void ExportDes(bool selectedOnly);
    void ClearFacetParams();
    void UpdateFacetParams(bool updateSelection=false);
    void ApplyFacetParams();

    //void UpdateRegionSelection();

    void StartStopSimulation();
    void SaveConfig();
    void LoadConfig();
	
    void UpdateFacetHits(bool all=false);

	void QuickPipe();

	void UpdatePlotters();
    void RefreshPlotterCombos();

	void ClearRegions();
	void RemoveRegion(int index);
	void NewRegion();

	void OpenEditor(const std::string &fileName);

    // Recent files   	
	char *recentPARs[MAX_RECENT];
    int  nbRecentPAR;
    void AddRecentPAR(const char *fileName);
    void RemoveRecentPAR(const char *fileName);
	void UpdateRecentPARMenu();
	void PlaceScatteringControls(bool newReflectionMode);

    // Components

	GLTextField   *doseNumber;
    
	GLTextField   *facetTeleport;
    GLTextField   *facetSticking;
	GLTextField   *facetRMSroughness;
	GLTextField   *facetAutoCorrLength;
	GLCombo       *facetReflType;
	GLToggle      *facetDoScattering;
    GLTextField   *facetStructure;
    GLCombo       *facetProfileCombo;
	GLToggle	  *facetSpectrumToggle;
    GLButton      *facetTexBtn;
    GLLabel       *modeLabel;
	GLLabel       *doseLabel;
	GLLabel       *facetTPLabel;
	GLLabel       *facetRMSroughnessLabel;
	GLLabel       *facetAutoCorrLengthLabel;
    GLLabel       *facetLinkLabel;
    GLLabel       *facetStructureLabel;
    GLTextField   *facetSuperDest;
   // GLLabel       *facetTLabel;
    GLLabel       *facetRLabel;
    GLLabel       *facetReLabel;
	GLLabel		*facetSpecLabel;

	GLMenu        *PARloadToMenu;
	GLMenu        *PARremoveMenu;
	GLMenu		  *ShowHitsMenu;

	//Materials (photon reflection)
	std::vector<std::string> materialPaths;

	void RebuildPARMenus();

    //Dialog
    RegionInfo       *regionInfo;
    //RegionSelection       *regionSelection;
    //ExportDesorption *exportDesorption;
    FacetMesh        *facetMesh;
    FacetDetails     *facetDetails;
	TrajectoryDetails *trajectoryDetails;
    Viewer3DSettings  *viewer3DSettings;
    TextureSettings  *textureSettings;
	GlobalSettings	 *globalSettings;
    ProfilePlotter   *profilePlotter;
	SpectrumPlotter  *spectrumPlotter;
    TexturePlotter   *texturePlotter;
	RegionEditor     *regionEditor;

    std::shared_ptr<std::set<size_t>> selectedRegions;

    //char *nbF;

    // Testing
    //int     nbSt;
    //void LogProfile();
    void BuildPipe(double ratio,int steps=0);
	void EmptyGeometry();
	
	void CrashHandler(Error *e);

protected:

    int  OneTimeSceneInit();
    int  RestoreDeviceObjects();
    int  InvalidateDeviceObjects();
	int  FrameMove();
    void ProcessMessage(GLComponent *src,int message);

};

