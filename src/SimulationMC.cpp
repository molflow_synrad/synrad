
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include "Simulation.h"
#include "IntersectAABB_shared.h"
#include "Random.h"
#include "SynradDistributions.h"
#include <vector>
#include "Region_mathonly.h"
#include "GeneratePhoton.h"
#include "Helper/MathTools.h"
#include "SynradTypes.h" //Histogram
#include <tuple>
#include <cstring>
#include <ProcessControl.h>

//extern Simulation *sHandle;
//extern void SetErrorSub(const char *message);

//extern Distribution2D K_1_3_distribution;
//extern Distribution2D K_2_3_distribution;
//extern DistributionND SR_spectrum_CDF;
extern Distribution2D integral_N_photons;
extern Distribution2D integral_SR_power;
//extern Distribution2D polarization_distribution;
//extern Distribution2D g1h2_distribution;

void Simulation::ComputeSourceArea() {
    sourceArea = wp.nbTrajPoints;
}

bool Simulation::UpdateMCHits(Dataport *dpHit, int prIdx, DWORD timeout) {

    BYTE *buffer;
    GlobalHitBuffer *gHits;
    TextureCell oldMin, oldMax;
    size_t i, j, s, x, y;
#ifdef _DEBUG
    double t0, t1;
    t0 = GetTick();
#endif
    //SetState(0, "Waiting for 'hits' dataport access...", false, true);
    bool lastHitUpdateOK = AccessDataportTimed(dpHit, timeout);
    //SetState(0, "Updating MC hits...", false, true);
    if (!lastHitUpdateOK) return false; //Timeout, will try again later

    buffer = (BYTE *) dpHit->buff;
    gHits = (GlobalHitBuffer *) buffer;

    // Global hits and leaks
    gHits->globalHits.hit.nbMCHit += tmpGlobalResult.globalHits.hit.nbMCHit;
    gHits->globalHits.hit.nbHitEquiv += tmpGlobalResult.globalHits.hit.nbHitEquiv;
    gHits->globalHits.hit.nbAbsEquiv += tmpGlobalResult.globalHits.hit.nbAbsEquiv;
    gHits->globalHits.hit.nbDesorbed += tmpGlobalResult.globalHits.hit.nbDesorbed;
    gHits->globalHits.hit.fluxAbs += tmpGlobalResult.globalHits.hit.fluxAbs;
    gHits->globalHits.hit.powerAbs += tmpGlobalResult.globalHits.hit.powerAbs;
    gHits->distTraveledTotal += currentParticle.distTraveledSinceUpdate;

    oldMin = gHits->hitMin;
    oldMax = gHits->hitMax;

    gHits->hitMin.count = HITMAX_UINT64;
    gHits->hitMax.count = 0;
    gHits->hitMin.flux = HITMAX_DOUBLE;
    gHits->hitMax.flux = 0;
    gHits->hitMin.power = HITMAX_DOUBLE;
    gHits->hitMax.power = 0;
    //for(i=0;i<BOUNCEMAX;i++) gHits->wallHits[i] += wallHits[i];

    // Leak
    for (size_t leakIndex = 0; leakIndex < tmpGlobalResult.leakCacheSize; leakIndex++)
        gHits->leakCache[(leakIndex + gHits->lastLeakIndex) % LEAKCACHESIZE] = tmpGlobalResult.leakCache[leakIndex];
    gHits->nbLeakTotal += nbLeakSinceUpdate;
    gHits->lastLeakIndex = (gHits->lastLeakIndex + tmpGlobalResult.leakCacheSize) % LEAKCACHESIZE;
    gHits->leakCacheSize = Min(LEAKCACHESIZE, gHits->leakCacheSize + tmpGlobalResult.leakCacheSize);

    // Hit cache (Only prIdx 0)
    if (prIdx == 0) {
        for (size_t hitIndex = 0; hitIndex < tmpGlobalResult.hitCacheSize; hitIndex++)
            gHits->hitCache[(hitIndex + gHits->lastHitIndex) % HITCACHESIZE] = tmpGlobalResult.hitCache[hitIndex];

        if (tmpGlobalResult.hitCacheSize > 0) {
            gHits->lastHitIndex = (gHits->lastHitIndex + tmpGlobalResult.hitCacheSize) % HITCACHESIZE;


            gHits->hitCache[gHits->lastHitIndex].type = HIT_LAST; //Penup (border between blocks of consecutive hits in the hit cache)


            gHits->hitCacheSize = Min(HITCACHESIZE, gHits->hitCacheSize + tmpGlobalResult.hitCacheSize);
        }
    }

    // Facets
    for (s = 0; s < sh.nbSuper; s++) {
        for (auto &f : structures[s].facets) {

            if (f.isHit) {

                FacetHitBuffer *fFit = (FacetHitBuffer *) (buffer + f.sh.hitOffset);
                *fFit += f.tmpCounter;

                if (f.sh.isProfile) {
                    ProfileSlice *shProfile = (ProfileSlice *) (buffer + (f.sh.hitOffset + sizeof(FacetHitBuffer)));
                    for (j = 0; j < PROFILE_SIZE; j++) {
                        shProfile[j] += f.profile[j];
                    }
                }

                size_t profileSize = (f.sh.isProfile) ? PROFILE_SIZE * sizeof(ProfileSlice) : 0;

                if (f.sh.isTextured) {
                    size_t nbTextureCells = f.sh.texHeight * f.sh.texWidth;
                    TextureCell *shTexture = (TextureCell *) (buffer +
                                                              (f.sh.hitOffset + sizeof(FacetHitBuffer) + profileSize));
                    for (y = 0; y < f.sh.texHeight; y++) {
                        for (x = 0; x < f.sh.texWidth; x++) {
                            size_t index = x + y * f.sh.texWidth;
                            //Increase value
                            shTexture[index] += f.texture[index];
                            //Adjust min/max
                            if (shTexture[index].count > gHits->hitMax.count)
                                gHits->hitMax.count = shTexture[index].count;
                            if (shTexture[index].count > 0 && shTexture[index].count < gHits->hitMin.count)
                                gHits->hitMin.count = shTexture[index].count;
                            if (shTexture[index].flux > gHits->hitMax.flux && f.largeEnough[index])
                                gHits->hitMax.flux = shTexture[index].flux;
                            if (shTexture[index].flux > 0.0 && shTexture[index].flux < gHits->hitMin.flux &&
                                f.largeEnough[index])
                                gHits->hitMin.flux = shTexture[index].flux;
                            if (shTexture[index].power > gHits->hitMax.power && f.largeEnough[index])
                                gHits->hitMax.power = shTexture[index].power;
                            if (shTexture[index].power > 0.0 && shTexture[index].power < gHits->hitMin.power &&
                                f.largeEnough[index])
                                gHits->hitMin.power = shTexture[index].power;
                        }
                    }
                }

                if (f.sh.countDirection) {
                    DirectionCell *shDir = (DirectionCell *) (buffer +
                                                              (f.sh.hitOffset + sizeof(FacetHitBuffer) + profileSize +
                                                               f.textureSize));
                    for (y = 0; y < f.sh.texHeight; y++) {
                        for (x = 0; x < f.sh.texWidth; x++) {
                            size_t add = x + y * f.sh.texWidth;
                            shDir[add].dir += f.direction[add].dir;
                            shDir[add].count += f.direction[add].count;
                        }
                    }
                }

                if (f.sh.recordSpectrum) {
                    ProfileSlice *shSpectrum = (ProfileSlice *) (buffer +
                                                                 (f.sh.hitOffset + sizeof(FacetHitBuffer) + profileSize
                                                                  + f.textureSize + f.directionSize));
                    for (j = 0; j < SPECTRUM_SIZE; j++) {
                        shSpectrum[j] += f.spectrum.GetCounts(j);
                    }
                }

            } // End if(hitted)
        } // End nbFacet
    } // End nbSuper

    //if there were no textures:
    if (gHits->hitMin.count == HITMAX_UINT64) gHits->hitMin.count = oldMin.count;
    if (gHits->hitMax.count == 0) gHits->hitMax.count = oldMax.count;
    if (gHits->hitMin.flux == HITMAX_DOUBLE) gHits->hitMin.flux = oldMin.flux;
    if (gHits->hitMax.flux == 0.0) gHits->hitMax.flux = oldMax.flux;
    if (gHits->hitMin.power == HITMAX_DOUBLE) gHits->hitMin.power = oldMin.power;
    if (gHits->hitMax.power == 0.0) gHits->hitMax.power = oldMax.power;

    // Another loop for a comlete global min/max texture search
    for (s = 0; s < sh.nbSuper; s++) {
        for (SubprocessFacet &f : structures[s].facets) {

            if (f.sh.isTextured) {
                {
                    // go on if the facet was never hit before
                    FacetHitBuffer *facetHitBuffer = (FacetHitBuffer *) (buffer + f.sh.hitOffset);
                    if (facetHitBuffer->hit.nbMCHit == 0 && facetHitBuffer->hit.nbDesorbed == 0) continue;
                }
                size_t profileSize = (f.sh.isProfile) ? PROFILE_SIZE * sizeof(ProfileSlice) : 0;
                TextureCell *shTexture = (TextureCell *) (buffer +
                                                          (f.sh.hitOffset + sizeof(FacetHitBuffer) + profileSize));

                for (y = 0; y < f.sh.texHeight; y++) {
                    for (x = 0; x < f.sh.texWidth; x++) {
                        size_t index = x + y * f.sh.texWidth;

                        //Add temporary hit counts
                        gHits->hitMax.count = std::max(gHits->hitMax.count, shTexture[index].count);
                        if (shTexture[index].count > 0)
                            gHits->hitMin.count = std::min(gHits->hitMin.count, shTexture[index].count);

                        if(f.largeEnough[index]) {
                            gHits->hitMax.flux = std::max(gHits->hitMax.flux, shTexture[index].flux);
                            if (shTexture[index].flux > 0.0)
                                gHits->hitMin.flux = std::min(gHits->hitMin.flux, shTexture[index].flux);

                            gHits->hitMax.power = std::max(gHits->hitMax.power, shTexture[index].power);
                            if (shTexture[index].power > 0.0)
                                gHits->hitMin.power = std::min(gHits->hitMin.power, shTexture[index].power);
                        } // if largeenough
                    }
                }
            }
        }
    }

    ReleaseDataport(dpHit);
    ResetTmpCounters();
    //SetState(PROCESS_STARTING, GetSimuStatus(), false, true);

#ifdef _DEBUG
    t1 = GetTick();
    printf("Update hits: %f us\n", (t1 - t0) * 1000000.0);
#endif

    return true;
}

void Simulation::UpdateLog(Dataport *dpLog, DWORD timeout) {
    if (tmpParticleLog.size()) {
#if defined(_DEBUG)
        double t0, t1;
        t0 = GetTick();
#endif
        //SetState(PROCESS_STARTING, "Waiting for 'dpLog' dataport access...", false, true);
        lastLogUpdateOK = AccessDataportTimed(dpLog, timeout);
        //SetState(PROCESS_STARTING, "Updating Log...", false, true);
        if (!lastLogUpdateOK) return;

        size_t *logBuff = (size_t *) dpLog->buff;
        size_t recordedLogSize = *logBuff;
        ParticleLoggerItem *logBuff2 = (ParticleLoggerItem *) (logBuff + 1);

        size_t writeNb;
        if (recordedLogSize > ontheflyParams.logLimit) writeNb = 0;
        else writeNb = Min(tmpParticleLog.size(), ontheflyParams.logLimit - recordedLogSize);
        memcpy(&logBuff2[recordedLogSize], &tmpParticleLog[0],
               writeNb * sizeof(ParticleLoggerItem)); //Knowing that vector memories are contigious
        (*logBuff) += writeNb;
        ReleaseDataport(dpLog);
        tmpParticleLog.clear();
        //SetState(PROCESS_STARTING, GetSimuStatus(), false, true);

#ifdef _DEBUG
        t1 = GetTick();
        printf("Update log: %f us\n", (t1 - t0) * 1000000.0);
#endif
    }
}

// Compute particle teleport

void Simulation::PerformTeleport(SubprocessFacet *collidedFacet) {

    //Search destination
    bool found = false;
    int destIndex;
    SubprocessFacet *destination;

    if (collidedFacet->sh.teleportDest == -1) {
        destIndex = currentParticle.teleportedFrom;
        if (destIndex == -1) {
            /*char err[128];
            sprintf(err, "Facet %d tried to teleport to the facet where the particle came from, but there is no such facet.", collidedFacet->globalId + 1);
            SetErrorSub(err);*/
            RecordHit(HIT_REF);
            currentParticle.lastHitFacet = collidedFacet;
            return; //LEAK
        }
    } else destIndex = collidedFacet->sh.teleportDest - 1;

    //Look in which superstructure is the destination facet:
    for (size_t i = 0; i < sh.nbSuper && (!found); i++) {
        for (size_t j = 0; j < structures[i].facets.size() && (!found); j++) {
            if (destIndex == structures[i].facets[j].globalId) {
                destination = &(structures[i].facets[j]);
                if (destination->sh.superIdx != -1) {
                    currentParticle.structureId = destination->sh.superIdx; //change current superstructure, unless universal facet
                }
                currentParticle.teleportedFrom = (int) collidedFacet->globalId; //memorize where the particle came from
                found = true;
            }
        }
    }
    if (!found) {
        /*char err[128];
        sprintf(err, "Teleport destination of facet %d not found (facet %d does not exist)", collidedFacet->globalId + 1, collidedFacet->sh.teleportDest);
        SetErrorSub(err);*/
        RecordHit(HIT_REF);
        currentParticle.lastHitFacet = collidedFacet;
        return; //LEAK
    }

    // Count this hit as a transparent pass
    RecordHit(HIT_TELEPORTSOURCE);
    if (collidedFacet->sh.countTrans) RecordHitOnTexture(collidedFacet, currentParticle.dF, currentParticle.dP);

    // Relaunch particle from new facet
    auto[inTheta, inPhi] = CartesianToPolar(currentParticle.direction, collidedFacet->sh.nU, collidedFacet->sh.nV,
                                            collidedFacet->sh.N);
    currentParticle.direction = PolarToCartesian(destination, inTheta, inPhi, false);

    // Move particle to teleport destination point
    currentParticle.position =
            destination->sh.O + collidedFacet->colU * destination->sh.U + collidedFacet->colV * destination->sh.V;

    RecordHit(HIT_TELEPORTDEST);
    currentParticle.lastHitFacet = destination;

    //Count hits on teleport facets (only TP source)
    //collidedFacet->counter.nbAbsorbed++;
    //destination->counter.nbDesorbed++;

    ProfileSlice increment;
    increment.count_absorbed = 0;
    increment.count_incident = 1;
    increment.flux_absorbed = 0.0;
    increment.flux_incident = currentParticle.dF;
    increment.power_absorbed = 0.0;
    increment.power_incident = currentParticle.dP;
    ProfileFacet(collidedFacet, currentParticle.energy, increment); //Put here since removed from Intersect() routine

    collidedFacet->tmpCounter.hit.nbMCHit++;//destination->counter.nbMCHit++;
    collidedFacet->tmpCounter.hit.nbHitEquiv += currentParticle.oriRatio;
    collidedFacet->tmpCounter.hit.fluxAbs += currentParticle.dF;//destination->counter.fluxAbs+=dF;
    collidedFacet->tmpCounter.hit.powerAbs += currentParticle.dP;//destination->counter.powerAbs+=dP;
}

// Perform nbStep simulation steps (a step is a bounce)
bool Simulation::SimulationMCStep(size_t nbStep) {

    // Check end of simulation
    if (ontheflyParams.desorptionLimit > 0) {
        if (totalDesorbed >= ontheflyParams.desorptionLimit / ontheflyParams.nbProcess) {
            //currentParticle.lastHitFacet = nullptr; // reset full particle status or go on from where we left
            return false;
        }
    }

    if (!currentParticle.lastHitFacet)
        if(!StartFromSource())
            return false; // desorp limit reached

    // Perform simulation steps
    for (size_t i = 0; i < nbStep; i++) {

        //std::tie(found,collidedFacetPtr,d) = Intersect(pPos, pDir); //May decide reflection type
        auto[found, collidedFacet, d] = Intersect(this, currentParticle.position, currentParticle.direction);

        if (found) {

            // Move particle to intersection point
            currentParticle.position = currentParticle.position + d * currentParticle.direction;
            currentParticle.distTraveledSinceUpdate += d;
            if (!ontheflyParams.logAbsorbedOnly) {
                LogHit(collidedFacet);
            }

            if (collidedFacet->sh.teleportDest) {
                PerformTeleport(collidedFacet); //increases hit, flux, power counters
            } else {
                // Record incident
                tmpGlobalResult.globalHits.hit.nbMCHit++;
                tmpGlobalResult.globalHits.hit.nbHitEquiv += currentParticle.oriRatio;
                collidedFacet->tmpCounter.hit.nbMCHit++;
                collidedFacet->tmpCounter.hit.nbHitEquiv += currentParticle.oriRatio;
                if (collidedFacet->sh.superDest) {    // Handle super structure link facet
                    currentParticle.structureId = collidedFacet->sh.superDest - 1;
                    // Count this hit as a transparent pass
                    RecordHit(HIT_TRANS);
                    ProfileSlice increment;
                    increment.count_absorbed = 0;
                    increment.count_incident = 1;
                    increment.flux_absorbed = 0.0;
                    increment.flux_incident = currentParticle.dF;
                    increment.power_absorbed = 0.0;
                    increment.power_incident = currentParticle.dP;
                    ProfileFacet(collidedFacet, currentParticle.energy, increment);

                    if (collidedFacet->sh.countTrans) {
                        RecordHitOnTexture(collidedFacet, currentParticle.dF, currentParticle.dP);
                    }
                } else { //Not superDest or Teleport
                    if (wp.newReflectionModel) {
                        //New reflection model (Synrad 1.4)
                        double inTheta, inPhi;
                        std::tie(inTheta, inPhi) = CartesianToPolar(currentParticle.direction, collidedFacet->sh.nU,
                                                                    collidedFacet->sh.nV, collidedFacet->sh.N);

                        double stickingProbability;
                        bool complexScattering; //forward/diffuse/back/transparent/stick
                        std::vector<double> materialReflProbabilities; //0: forward reflection, 1: diffuse, 2: backscattering, 3: transparent, 100%-(0+1+2+3): absorption

                        if (currentParticle.dF == 0.0 || currentParticle.dP == 0.0 || currentParticle.energy < 1E-3) {
                            //stick non real photons (from beam beginning)
                            stickingProbability = 1.0;
                            complexScattering = false;
                        } else if (collidedFacet->sh.reflectType < 2) {
                            //Diffuse or Mirror
                            stickingProbability = collidedFacet->sh.sticking;
                            complexScattering = false;
                        } else { //Material
                            std::tie(stickingProbability, materialReflProbabilities,
                                     complexScattering) = GetStickingProbability(*collidedFacet, inTheta);
                            //In the new reflection model, reflection probabilities don't take into account surface roughness
                        }

                        if (!ontheflyParams.lowFluxMode) {
                            //Regular mode, stick or bounce

                            //First register incident on profile
                                ProfileSlice increment;
                                increment.count_absorbed = 0;
                                increment.count_incident = 1;
                                increment.flux_absorbed = 0.0;
                                increment.flux_incident = currentParticle.dF;
                                increment.power_absorbed = 0.0;
                                increment.power_incident = currentParticle.dP;
                                ProfileFacet(collidedFacet, currentParticle.energy, increment);
                            int reflType = GetHardHitType(stickingProbability, materialReflProbabilities,
                                                          complexScattering);
                            if (reflType == REFL_ABSORB) {
                                Stick(*collidedFacet);
                                if (!StartFromSource()) return false;
                            }
                            else {
                                PerformBounce_new(*collidedFacet, reflType, inTheta, inPhi);
                            }
                        } else {
                            //Low flux mode and simple scattering:
                            Vector3d dummyNullVector(0.0, 0.0,
                                                     0.0); //DoLowFluxReflection will not use it since wp.newReflectionModel == true
                            DoLowFluxReflection(*collidedFacet, stickingProbability, complexScattering,
                                                materialReflProbabilities,
                                                inTheta, inPhi, dummyNullVector, dummyNullVector, dummyNullVector); //registers incident profile
                        } //end low flux mode
                    } else {
                        //Old reflection model (Synrad <=1.3 or if user deselects new model in Global Settings)
                        if (currentParticle.dF == 0.0 || currentParticle.dP == 0.0 ||
                            currentParticle.energy < 1E-3) { //stick non real photons (from beam beginning)
                            Stick(*collidedFacet);
                            if (!StartFromSource()) return false;
                        } else {
                            //We first generate a perturbated surface, and from that point on we treat the whole reflection process as if it were locally flat


                            if ((collidedFacet->sh.reflectType - 10) < (int) materials.size()) { //found material type
                                //Generate incident angle
                                Vector3d nU_rotated, N_rotated, nV_rotated;
                                bool reflected = false;
                                do { //generate surfaces until reflected ray goes away from facet (and not through it)

                                    //First step: generate a random surface and determine incident angles
                                    double inTheta, inPhi;
                                    if (collidedFacet->sh.doScattering) {
                                        double n_ori = Dot(currentParticle.direction,
                                                           collidedFacet->sh.N); //incident angle with original facet surface (negative if front collision, positive if back collision);
                                        double n_new;
                                        do { //generate angles until incidence is from front
                                            double sigmaRatio =
                                                    collidedFacet->sh.rmsRoughness / collidedFacet->sh.autoCorrLength;
                                            std::tie(nU_rotated, nV_rotated, N_rotated) = PerturbateSurface(
                                                    *collidedFacet, sigmaRatio);
                                            n_new = Dot(currentParticle.direction, N_rotated);
                                        } while (n_new * n_ori <=
                                                 0.0); //generate new random surface if grazing angle would be over 90deg (shadowing)
                                        std::tie(inTheta, inPhi) = CartesianToPolar(currentParticle.direction,
                                                                                    nU_rotated, nV_rotated,
                                                                                    N_rotated); //Finally, get incident angles
                                    } else { //no scattering, use original surface
                                        nU_rotated = collidedFacet->sh.nU;
                                        nV_rotated = collidedFacet->sh.nV;
                                        N_rotated = collidedFacet->sh.N;
                                        std::tie(inTheta, inPhi) = CartesianToPolar(currentParticle.direction,
                                                                                    collidedFacet->sh.nU,
                                                                                    collidedFacet->sh.nV,
                                                                                    collidedFacet->sh.N); //Incident angles with original facet surface
                                    }

                                    //Second step: determine sticking/scattering probabilities
                                    double stickingProbability;
                                    std::vector<double> materialReflProbabilities; //vector of fwd/diffuse/back/transparent scattering probs
                                    bool complexScattering; //does the material support multiple (diffuse, back, transparent) reflection modes?

                                    if (collidedFacet->sh.reflectType < 2) {
                                        //diffuse or mirror with fixed probability
                                        stickingProbability = collidedFacet->sh.sticking;
                                        complexScattering = false;
                                    } else {
                                        //material reflection, depends on incident angle and energy
                                        std::tie(stickingProbability, materialReflProbabilities,
                                                 complexScattering) = GetStickingProbability(*collidedFacet, inTheta);
                                    }
                                    if (!ontheflyParams.lowFluxMode) {
                                        //Regular Monte-Carlo, stick or reflect fwd/diff/back/through
                                        int reflType = GetHardHitType(stickingProbability, materialReflProbabilities,
                                                                      complexScattering);
                                        reflected = DoOldRegularReflection(*collidedFacet, reflType, inTheta, inPhi,
                                                                           N_rotated, nU_rotated, nV_rotated);
                                    } else {
                                        //Low flux mode
                                        reflected = DoLowFluxReflection(*collidedFacet, stickingProbability,
                                                                        complexScattering, materialReflProbabilities,
                                                                        inTheta, inPhi,
                                                                        N_rotated, nU_rotated, nV_rotated);
                                    }
                                } while (!reflected); //do it again if reflection wasn't successful (reflected against the surface due to roughness)
                            } else {
                                std::string err = "Facet " + std::to_string(collidedFacet->globalId + 1);
                                err += "reflection material type not found";
                                //SetErrorSub(err.c_str());
                                std::cerr << err << std::endl;
                            }
                        }

                    }
                }
            }
        } else { // Leak (simulation error)
            nbLeakSinceUpdate++;
            if (regions[sourceRegionId].params.showPhotons) {
                RecordLeakPos();
            }
            if (!StartFromSource())
                // desorptionLimit reached
                return false;
        } //end intersect or leak
    } //end step
    return true;
}

std::tuple<double, std::vector<double>, bool>
Simulation::GetStickingProbability(const SubprocessFacet &collidedFacet, const double &theta) {
    //Returns sticking probability, materialReflProbabilities and complexScattering
    Material *mat = &(materials[collidedFacet.sh.reflectType - 10]);
    bool complexScattering = mat->hasBackscattering;
    std::vector<double> materialReflProbabilities = mat->BilinearInterpolate(currentParticle.energy,
                                                                             abs(theta - PI / 2));
    double stickingProbability = complexScattering
                                 ? 1.0 - materialReflProbabilities[0] - materialReflProbabilities[1] -
                                   materialReflProbabilities[2] //100% - forward - diffuse - back (transparent already excluded in Intersect() routine)
                                 : 1.0 -
                                   materialReflProbabilities[0]; //100% - forward (transparent already excluded in Intersect() routine)
    return std::make_tuple(stickingProbability, materialReflProbabilities, complexScattering);
}

int Simulation::GetHardHitType(const double &stickingProbability, const std::vector<double> &materialReflProbabilities,
                               const bool &complexScattering) {
    //Similar to Material::GetReflectionType, but transparent pass is excluded and treats Mirror/Diffuse surfaces too with single sticking factor
    double nonTransparentProbability = (complexScattering) ? 1.0 - materialReflProbabilities[3] : 1.0;
    double random = randomGenerator.rnd() * nonTransparentProbability;
    if (random > (nonTransparentProbability - stickingProbability)) return REFL_ABSORB;
    else if (!complexScattering) {
        return REFL_FORWARD; //Not absorbed, so reflected
    } else { //complex scattering
        if (random < materialReflProbabilities[0]) return REFL_FORWARD; //forward reflection
        else if (random < (materialReflProbabilities[0] + materialReflProbabilities[1])) return REFL_DIFFUSE;
        else if (random <
                 (materialReflProbabilities[0] + materialReflProbabilities[1] + materialReflProbabilities[2]))
            return REFL_BACK;
        else return REFL_BACK; //should never be the case. Trasparent already excluded in Intersect() routine, and in this "else" branch we know that it doesn't stick
    }
}

std::tuple<Vector3d, Vector3d, Vector3d>
Simulation::PerturbateSurface(const SubprocessFacet &collidedFacet, const double &sigmaRatio) {

    double rnd1 = randomGenerator.rnd();
    double rnd2 = randomGenerator.rnd(); //for debug
    //Saturate(rnd1, 0.01, 0.99); //Otherwise thetaOffset would go to +/- infinity
    //Saturate(rnd2, 0.01, 0.99); //Otherwise phiOffset would go to +/- infinity
    double thetaOffset = atan(sigmaRatio * tan(PI * (rnd1 - 0.5)));
    double phiOffset = atan(sigmaRatio * tan(PI * (rnd2 - 0.5)));

    //Make a local copy
    Vector3d nU_facet = collidedFacet.sh.nU;
    Vector3d nV_facet = collidedFacet.sh.nV;
    Vector3d N_facet = collidedFacet.sh.N;

    //Random rotation around N (to discard U orientation thus make scattering isotropic)
    double randomAngle = randomGenerator.rnd() * 2 * PI;
    nU_facet = Rotate(nU_facet, Vector3d(0, 0, 0), N_facet, randomAngle);
    nV_facet = Rotate(nV_facet, Vector3d(0, 0, 0), N_facet, randomAngle);

    //Bending surface base vectors
    Vector3d nU_rotated = Rotate(nU_facet, Vector3d(0, 0, 0), nV_facet, thetaOffset);
    Vector3d N_rotated = Rotate(N_facet, Vector3d(0, 0, 0), nV_facet, thetaOffset);
    nU_rotated = Rotate(nU_rotated, Vector3d(0, 0, 0), nU_facet, phiOffset); //check if correct
    Vector3d nV_rotated = Rotate(nV_facet, Vector3d(0, 0, 0), nU_facet, phiOffset);
    N_rotated = Rotate(N_rotated, Vector3d(0, 0, 0), nU_facet, phiOffset);

    return std::make_tuple(nU_rotated, nV_rotated, N_rotated);
}

/*
std::tuple<double,double,double> GetDirComponents(const Vector3d& nU_rotated, const Vector3d& nV_rotated, const Vector3d& N_rotated) {
	double u = Dot(pDir, nU_rotated);
	double v = Dot(pDir, nV_rotated);
	double n = Dot(pDir, N_rotated);
	Saturate(n, -1.0, 1.0); //sometimes rounding errors do occur, 'acos' function would return no value for theta
	return std::make_tuple(u, v, n);
}*/

bool Simulation::DoLowFluxReflection(SubprocessFacet &collidedFacet, const double &stickingProbability,
                                     const bool &complexScattering,
                                     const std::vector<double> &materialReflProbabilities,
                                     const double &inTheta, const double &inPhi,
                                     const Vector3d &N_rotated, const Vector3d &nU_rotated,
                                     const Vector3d &nV_rotated) {

    //First register sticking part:
    collidedFacet.tmpCounter.hit.fluxAbs += currentParticle.dF * stickingProbability;
    collidedFacet.tmpCounter.hit.powerAbs += currentParticle.dP * stickingProbability;
    collidedFacet.tmpCounter.hit.nbAbsEquiv += currentParticle.oriRatio * stickingProbability;
    if (collidedFacet.sh.countAbs) {
        RecordHitOnTexture(&collidedFacet,currentParticle.dF * stickingProbability, currentParticle.dP * stickingProbability);
    }
    ProfileSlice increment;
    increment.count_absorbed = 0;
    increment.count_incident = 1;
    increment.flux_absorbed = currentParticle.dF * stickingProbability;
    increment.flux_incident = currentParticle.dF;
    increment.power_absorbed = currentParticle.dP * stickingProbability;
    increment.power_incident = currentParticle.dP;
    ProfileFacet(&collidedFacet, currentParticle.energy, increment);
    //Absorbed part recorded, let's see how much is left
    double survivalProbability = 1.0 - stickingProbability;
    
    if (ontheflyParams.logAbsorbedOnly) { //incident part wasn't logged
        double oriRatio_memorized = currentParticle.oriRatio;
        double dF_memorized = currentParticle.dF;
        double dP_memorized = currentParticle.dP;
        currentParticle.oriRatio *= stickingProbability; //temporarily set to absorbed part
        currentParticle.dF *= stickingProbability;
        currentParticle.dP *= stickingProbability;
        LogHit(&collidedFacet); //record absorbed part
        currentParticle.oriRatio = oriRatio_memorized; //restore memorized
        currentParticle.dF = dF_memorized;
        currentParticle.dP = dP_memorized;
    }
    currentParticle.oriRatio *= survivalProbability; //Apply reflected part
    if (currentParticle.oriRatio < ontheflyParams.lowFluxCutoff) {//reflected part not important, throw it away
        RecordHit(HIT_ABS); //for hits and lines display
        return StartFromSource(); //false if maxdesorption reached
    } else { //reflect remainder
        currentParticle.dF *= survivalProbability;
        currentParticle.dP *= survivalProbability;

        //Decide reflection type (fwd/diff/back, transparent excluded by intersect)
        int reflType;
        if (!complexScattering) reflType = REFL_FORWARD;
        else {
            //Like GetHardHitType() but already excluding absorption
            double anyReflectionProbability =
                    survivalProbability - materialReflProbabilities[3]; //Not absorbed, not transparent
            double random = randomGenerator.rnd() * anyReflectionProbability;
            if (random < materialReflProbabilities[0]) reflType = REFL_FORWARD;
            else if (random < (materialReflProbabilities[0] + materialReflProbabilities[1])) reflType = REFL_DIFFUSE;
            else reflType = REFL_BACK;
        }

        if (wp.newReflectionModel) {
            PerformBounce_new(collidedFacet, reflType, inTheta, inPhi);
            return true;
        } else {
            return PerformBounce_old(collidedFacet, reflType, inTheta, inPhi, N_rotated, nU_rotated, nV_rotated);
        }
    }
}

bool Simulation::DoOldRegularReflection(SubprocessFacet &collidedFacet, const int &reflType, const double &theta,
                                        const double &phi,
                                        const Vector3d &N_rotated, const Vector3d &nU_rotated,
                                        const Vector3d &nV_rotated) {

    //currentParticle.lastHitFacet = &collidedFacet; //If sticks, startfromsource will set to NULL, if reflects, PerformBounce_old will set it
    ProfileSlice increment;
    increment.count_absorbed = 0;
    increment.count_incident = 1;
    increment.flux_absorbed = 0.0;
    increment.flux_incident = currentParticle.dF;
    increment.power_absorbed = 0.0;
    increment.power_incident = currentParticle.dP;
    ProfileFacet(&collidedFacet, currentParticle.energy, increment);
    //if (collidedFacet.sh.countRefl) RecordHitOnTexture(&collidedFacet, currentParticle.dF, currentParticle.dP); //Commented out: part of PerformBounce_old
    if (reflType == REFL_ABSORB) {
        Stick(collidedFacet);
        return StartFromSource(); //false if maxdesorption reached
    } else {
        return PerformBounce_old(collidedFacet, reflType, theta, phi, N_rotated, nU_rotated, nV_rotated);
    }
}

// Launch photon from a trajectory point

bool Simulation::StartFromSource() {

    // Check end of simulation
    if (ontheflyParams.desorptionLimit > 0) {
        if (totalDesorbed >= ontheflyParams.desorptionLimit / ontheflyParams.nbProcess) {
            //currentParticle.lastHitFacet = nullptr; // reset full particle status or go on from where we left
            return false;
        }
    }

    //find source point
    size_t pointIdGlobal = (size_t) (randomGenerator.rnd() * (double) sourceArea);
    bool found = false;
    size_t regionId;
    size_t pointIdLocal;
    size_t sum = 0;
    for (regionId = 0; regionId < wp.nbRegion && !found; regionId++) {
        if ((pointIdGlobal >= sum) && (pointIdGlobal < (sum + regions[regionId].Points.size()))) {
            pointIdLocal = pointIdGlobal - sum;
            found = true;
        } else sum += regions[regionId].Points.size();
    }
    if (!found) std::cerr<<"No start point found"<<std::endl;//SetErrorSub("No start point found");
    regionId--;
    //Trajectory_Point *source=&(regions[regionId].Points[pointIdLocal]);
    Region_mathonly *sourceRegion = &(regions[regionId]);
    if (!(sourceRegion->params.psimaxX_rad > 0.0 && sourceRegion->params.psimaxY_rad > 0.0))
        std::cerr<<"psiMaxX or psiMaxY not positive. No photon can be generated"<<std::endl; //SetErrorSub("psiMaxX or psiMaxY not positive. No photon can be generated");

    size_t retries = 0;
    bool validEnergy;
    GenPhoton photon;
    do {
        photon = GeneratePhoton(pointIdLocal, sourceRegion, ontheflyParams.generation_mode,
                                psi_distro, chi_distros[sourceRegion->params.polarizationCompIndex],
                                parallel_polarization, randomGenerator,
                                tmpGlobalResult.globalHits.hit.nbDesorbed == 0);
        validEnergy = (photon.energy >= sourceRegion->params.energy_low_eV &&
                       photon.energy <= sourceRegion->params.energy_hi_eV);
        if (!validEnergy && photon.energy > 0.0) {
            retries++;
            pointIdLocal = (size_t) (randomGenerator.rnd() * (double) sourceRegion->Points.size());
        }
    } while (!validEnergy && photon.energy > 0.0 && retries < 5);

    if (!validEnergy && photon.energy > 0.0) {
        char tmp[1024];
        sprintf(tmp, "Region %zd point %zd: can't generate within energy limits (%geV .. %geV)", regionId + 1,
                pointIdLocal + 1,
                sourceRegion->params.energy_low_eV, sourceRegion->params.energy_hi_eV);
        //SetErrorSub(tmp);
        std::cerr << tmp<<std::endl;
        return false;
    }

    //distTraveledCurrentParticle=0.0;
    currentParticle.dF = photon.SR_flux;
    currentParticle.dP = photon.SR_power;
    currentParticle.energy = photon.energy;
    currentParticle.oriRatio = 1.0;

    //starting position
    currentParticle.position = photon.start_pos;

    sourceRegionId = regionId;

    RecordHit(HIT_DES);

    //angle
    currentParticle.direction = photon.start_dir;

    if (sourceRegion->params.structureId > sh.nbSuper) {
        char tmp[1024];
        sprintf(tmp, "Region %zd is in structure %zd which doesn't exist\n(This geometry has only %zd structures)",
                regionId + 1, sourceRegion->params.structureId + 1, sh.nbSuper);
        //SetErrorSub(tmp);
        std::cerr << tmp<<std::endl;

        return false;
    }

    currentParticle.structureId = sourceRegion->params.structureId;
    currentParticle.teleportedFrom = -1;

    // Count
    totalDesorbed++;
    tmpGlobalResult.globalHits.hit.fluxAbs += currentParticle.dF;
    tmpGlobalResult.globalHits.hit.powerAbs += currentParticle.dP;
    tmpGlobalResult.globalHits.hit.nbDesorbed++;

    currentParticle.lastHitFacet = NULL; //Photon originates from the volume, not from a facet

    return true;

}

// Compute bounce against a facet

void Simulation::PerformBounce_new(SubprocessFacet &collidedFacet, const int &reflType, const double &inTheta,
                                   const double &inPhi) {

    double outTheta, outPhi; //perform bounce without scattering, will perturbate these angles later if it's a rough surface
    if (collidedFacet.sh.reflectType == REFLECTION_DIFFUSE) {
        outTheta = acos(sqrt(randomGenerator.rnd()));
        outPhi = randomGenerator.rnd() * 2.0 * PI;
    } else if (collidedFacet.sh.reflectType == REFLECTION_SPECULAR) {
        outTheta = PI - inTheta;
        outPhi = inPhi;
    } else { //material reflection, might have backscattering
        switch (reflType) {
            case REFL_FORWARD: //forward scattering
                outTheta = PI - inTheta;
                outPhi = inPhi;
                break;
            case REFL_DIFFUSE: //diffuse scattering
                outTheta = acos(sqrt(randomGenerator.rnd()));
                outPhi = randomGenerator.rnd() * 2.0 * PI;
                break;
            case REFL_BACK: //back scattering
                outTheta = PI - inTheta;
                outPhi = PI + inPhi;
                break;
        } //end switch (transparent pass treated at the Intersect() routine
    } //end material reflection

    if (collidedFacet.sh.doScattering) {
        double incidentAngle = abs(inTheta);
        if (incidentAngle > PI / 2) incidentAngle = PI - incidentAngle; //coming from the normal side
        double y = cos(incidentAngle);
        double wavelength = 3E8 * 6.626E-34 / (currentParticle.energy * 1.6E-19); //energy[eV] to wavelength[m]
        double specularReflProbability = exp(-Sqr(4 * PI * collidedFacet.sh.rmsRoughness * y /
                                                  wavelength)); //Debye-Wallers factor, See "Measurements of x-ray scattering..." by Dugan, Sonnad, Cimino, Ishibashi, Scafers, eq.2
        bool specularReflection = randomGenerator.rnd() < specularReflProbability;
        if (!specularReflection) {
            //Smooth surface reflection performed, now let's perturbate the angles
            //Using Gaussian approximated distributions of eq.14. of the above article
            double onePerTau = collidedFacet.sh.rmsRoughness / collidedFacet.sh.autoCorrLength;

            //Old acceptance-rejection algorithm
            /*
            size_t nbTries = 0; double outThetaPerturbated;
            do {
                double dTheta = Gaussian(2.9264*onePerTau); //Grazing angle perturbation, depends only on roughness, must assure it doesn't go against the surface
                outThetaPerturbated = outTheta + dTheta;
                nbTries++;
            } while (((outTheta < PI / 2) != (outThetaPerturbated < PI / 2)) && nbTries < 10);*/


            //New truncated Gaussian algorithm, see N. Chopin: Fast simulation of truncated Gaussian distributions, DOI: 10.1007/s11222-009-9168-1

            double lowerBound = 0.0;
            double upperBound = PI / 2;
            if (outTheta > (PI / 2)) { //Limits: PI/2 .. PI instead of 0..PI/2
                lowerBound += PI / 2;
                upperBound += PI / 2;
            }
            double outThetaPerturbated = truncated_normal_ab_sample(outTheta, 2.9264 * onePerTau, lowerBound,
                                                                       upperBound, rnd_seed);

            double dPhi = randomGenerator.Gaussian(
                    (2.80657 * pow(incidentAngle, -1.00238) - 1.00293 * pow(incidentAngle, 1.22425)) *
                    onePerTau); //Out-of-plane angle perturbation, depends on roughness and incident angle
            outTheta = outThetaPerturbated;
            outPhi += dPhi;
        }
    }

    currentParticle.direction = PolarToCartesian(&collidedFacet, outTheta, outPhi, false);

    RecordHit(HIT_REF);
    currentParticle.lastHitFacet = &collidedFacet;
    if (collidedFacet.sh.countRefl) {
        RecordHitOnTexture(&collidedFacet, currentParticle.dF, currentParticle.dP);
    }
}

bool Simulation::PerformBounce_old(SubprocessFacet &collidedFacet, const int &reflType, const double &inTheta,
                                   const double &inPhi,
                                   const Vector3d &N_rotated, const Vector3d &nU_rotated, const Vector3d &nV_rotated) {
    RecordHit(HIT_REF);
    if (collidedFacet.sh.countRefl) {
        RecordHitOnTexture(&collidedFacet,currentParticle.dF,currentParticle.dP);
    }
    // Relaunch particle, regular monte-carlo
    if (collidedFacet.sh.reflectType == REFLECTION_DIFFUSE) {
        //See docs/theta_gen.png for further details on angular distribution generation
        currentParticle.direction = PolarToCartesian(&collidedFacet, acos(sqrt(randomGenerator.rnd())),
                                                     randomGenerator.rnd() * 2.0 * PI, false);
    } else { //Fwd/diff/back reflection, optionally with surface perturbation
        if (!VerifiedSpecularReflection(collidedFacet,
                                        (collidedFacet.sh.reflectType == REFLECTION_SPECULAR) ? REFL_FORWARD : reflType,
                                        inTheta, inPhi,
                                        nU_rotated, nV_rotated, N_rotated)) {
            return false;
        }
    }
    currentParticle.lastHitFacet = &collidedFacet;
    return true;
}

bool
Simulation::VerifiedSpecularReflection(const SubprocessFacet &collidedFacet, const int &reflType, const double &inTheta,
                                       const double &inPhi,
                                       const Vector3d &nU_rotated, const Vector3d &nV_rotated,
                                       const Vector3d &N_rotated) {

    //Specular reflection that returns false if going against surface
    //Changes pDir

    double outTheta, outPhi;

    //Vector3d N_facet = Vector3d(collidedFacet->sh.N.x, collidedFacet->sh.N.y, collidedFacet->sh.N.z);

    Vector3d newDir;
    double u, v, n;
    bool calcNewDir = true;

    switch (reflType) {
        case REFL_FORWARD: //forward scattering
            outTheta = PI - inTheta;
            outPhi = inPhi;
            break;
        case REFL_DIFFUSE: //diffuse scattering
            outTheta = acos(sqrt(randomGenerator.rnd()));
            outPhi = randomGenerator.rnd() * 2.0 * PI;
            break;
        case REFL_BACK: //back scattering

            //we need to perturbate the backscattered ray with the angle difference between the original and the rotated surface

            //Get rotation matrix that transforms collidedFacet->sh.N into N_rotated
            //See https://math.stackexchange.com/questions/180418/calculate-rotation-matrix-to-align-vector-a-to-vector-b-in-3d

            Vector3d v = CrossProduct(collidedFacet.sh.N, N_rotated);
            double c = Dot(collidedFacet.sh.N, N_rotated);

            double factor = 1.0 / (1.0 + c);

            std::vector<std::vector<double>> nullMatrix = {{0.0, 0.0, 0.0},
                                                           {0.0, 0.0, 0.0},
                                                           {0.0, 0.0, 0.0}};
            std::vector<std::vector<double>> identityMatrix = {{1.0, 0.0, 0.0},
                                                               {0.0, 1.0, 0.0},
                                                               {0.0, 0.0, 1.0}}; //rows
            std::vector<std::vector<double>> v_skew = {{0.0, -v.z, v.y},
                                                       {v.z, 0.0,  -v.x},
                                                       {-v.y, v.x, 0.0}};

            std::vector<std::vector<double>> v_skew_square = nullMatrix;
            //3x3 matrix multiplication
            for (size_t row = 0; row < 3; row++) {
                for (size_t col = 0; col < 3; col++) {
                    for (size_t comp = 0; comp < 3; comp++) {
                        v_skew_square[row][col] += v_skew[row][comp] * v_skew[comp][col];
                    }
                }
            }

            std::vector<std::vector<double>> rotationMatrix = nullMatrix;
            for (size_t row = 0; row < 3; row++) {
                for (size_t col = 0; col < 3; col++) {
                    rotationMatrix[row][col] =
                            identityMatrix[row][col] + v_skew[row][col] + factor * v_skew_square[row][col];
                }
            }

            newDir = Vector3d(-currentParticle.direction.x * rotationMatrix[0][0] +
                              -currentParticle.direction.y * rotationMatrix[0][1] +
                              -currentParticle.direction.z * rotationMatrix[0][2],
                              -currentParticle.direction.x * rotationMatrix[1][0] +
                              -currentParticle.direction.y * rotationMatrix[1][1] +
                              -currentParticle.direction.z * rotationMatrix[1][2],
                              -currentParticle.direction.x * rotationMatrix[2][0] +
                              -currentParticle.direction.y * rotationMatrix[2][1] +
                              -currentParticle.direction.z * rotationMatrix[2][2]);

            calcNewDir = false;
            break;
    } //end switch (transparent pass treated at the Intersect() routine

    if (calcNewDir) {

        u = sin(outTheta) * cos(outPhi);
        v = sin(outTheta) * sin(outPhi);
        n = cos(outTheta);

        newDir = u * nU_rotated + v * nV_rotated + n * N_rotated;

    }

    if ((Dot(newDir, collidedFacet.sh.N) > 0) != (inTheta > PI / 2)) {
        //inTheta > PI/2: ray coming from normal side
        //Dot(newDir,N_facet)>0: ray leaving towards normal side
        return false; //if reflection would go against the surface, generate new angles
    }

    currentParticle.direction = newDir;
    return true;
}

void Simulation::RecordHitOnTexture(SubprocessFacet *f, double dF, double dP) {
    size_t tu = std::min(f->sh.texWidth-1,(size_t) (f->colU * f->sh.texWidth_precise)); //avoid colU==1.0 overshoot
    size_t tv = std::min(f->sh.texHeight-1,(size_t) (f->colV * f->sh.texHeight_precise)); //avoid colV==1.0 overshoot
    size_t index = tu + tv * f->sh.texWidth;
    f->texture[index].count++;
    f->texture[index].flux += dF * f->textureCellIncrements[index]; //normalized by area
    f->texture[index].power += dP * f->textureCellIncrements[index]; //normalized by area
}

void Simulation::RecordDirectionVector(SubprocessFacet *f) {
    size_t tu = (size_t) (f->colU * f->sh.texWidth_precise);
    size_t tv = (size_t) (f->colV * f->sh.texHeight_precise);
    size_t add = tu + tv * (f->sh.texWidth);

    f->direction[add].dir.x += currentParticle.direction.x;
    f->direction[add].dir.y += currentParticle.direction.y;
    f->direction[add].dir.z += currentParticle.direction.z;
    f->direction[add].count++;
}

void Simulation::Stick(SubprocessFacet &collidedFacet) {
    collidedFacet.tmpCounter.hit.nbAbsEquiv += 1.0;
    collidedFacet.tmpCounter.hit.fluxAbs += currentParticle.dF;
    collidedFacet.tmpCounter.hit.powerAbs += currentParticle.dP;
    tmpGlobalResult.globalHits.hit.nbAbsEquiv += 1.0;
    //distTraveledSinceUpdate+=distTraveledCurrentParticle;
    //counter.nbAbsorbed++;
    //counter.fluxAbs+=dF;
    //counter.powerAbs+=dP;
    RecordHit(HIT_ABS); //for hits and lines display
    ProfileSlice increment;
    increment.count_absorbed = 1;
    increment.count_incident = 0; //Already counted by the time Stick() is called
    increment.flux_absorbed = currentParticle.dF;
    increment.flux_incident = 0.0; //Already counted by the time Stick() is called
    increment.power_absorbed = currentParticle.dP;
    increment.power_incident = 0.0; //Already counted by the time Stick() is called
    ProfileFacet(&collidedFacet, currentParticle.energy, increment);
    if (collidedFacet.sh.countAbs) {
        RecordHitOnTexture(&collidedFacet, currentParticle.dF, currentParticle.dP);
    }
    if (ontheflyParams.logAbsorbedOnly) {
        LogHit(&collidedFacet);
    }
}


void Simulation::LogHit(SubprocessFacet *f) {
    if (ontheflyParams.enableLogging &&
        ontheflyParams.logFacetId == f->globalId &&
        tmpParticleLog.size() < (ontheflyParams.logLimit / ontheflyParams.nbProcess))
    {
        ParticleLoggerItem log;
        log.facetHitPosition = Vector2d(f->colU, f->colV);
        std::tie(log.hitTheta, log.hitPhi) = CartesianToPolar(currentParticle.direction, f->sh.nU, f->sh.nV, f->sh.N);
        log.oriRatio = currentParticle.oriRatio;
        log.energy = currentParticle.energy;
        log.dF = currentParticle.dF;
        log.dP = currentParticle.dP;
        tmpParticleLog.push_back(log);
    }
}

void Simulation::RegisterTransparentPass(SubprocessFacet *facet) {
    //Low flux mode not supported (ray properties can't change on transparent pass since it's inside the Intersect() routine)
    facet->isHit = true;
    facet->tmpCounter.hit.nbMCHit++; //count MC hits
    facet->tmpCounter.hit.nbHitEquiv += currentParticle.oriRatio;
    facet->tmpCounter.hit.fluxAbs += currentParticle.dF;
    facet->tmpCounter.hit.powerAbs += currentParticle.dP;
    ProfileSlice increment;
    increment.count_absorbed = 0;
    increment.count_incident = 1;
    increment.flux_absorbed = 0.0;
    increment.flux_incident = currentParticle.dF;
    increment.power_absorbed = 0.0;
    increment.power_incident = currentParticle.dP;
    ProfileFacet(facet, currentParticle.energy, increment); //count profile
    if (!ontheflyParams.logAbsorbedOnly) {
        LogHit(facet);
    }
    if (facet->sh.countTrans) RecordHitOnTexture(facet, currentParticle.dF, currentParticle.dP); //count texture
    if (facet->sh.countDirection) RecordDirectionVector(facet);
}

void Simulation::ProfileFacet(SubprocessFacet *f, const double &energy, const ProfileSlice &increment) {

    size_t pos;

    switch (f->sh.profileType) {

        case PROFILE_ANGULAR: {
            double dot = abs(Dot(f->sh.N, currentParticle.direction));
            double theta = acos(dot);              // Angle to normal (0 to PI/2)
            size_t grad = (size_t) (((double) PROFILE_SIZE) * (theta) / (PI / 2)); // To Grad
            Saturate(grad, 0, PROFILE_SIZE - 1);
            f->profile[grad] += increment;

        }
            break;

        case PROFILE_U:
            pos = (size_t) ((f->colU) * (double) PROFILE_SIZE);
            Saturate(pos, 0, PROFILE_SIZE - 1);
            f->profile[pos] += increment;
            break;

        case PROFILE_V:
            pos = (size_t) ((f->colV) * (double) PROFILE_SIZE);
            Saturate(pos, 0, PROFILE_SIZE - 1);
            f->profile[pos] += increment;
            break;

    }

    if (f->sh.recordSpectrum) {
        f->spectrum.Add(energy, increment);
    }
}

