
#ifndef _QUADRUPOLE_
#define _QUADRUPOLE_

enum quadType {
    regular,
    combined,
    curved
};

class Quadrupole {
public:
    //MAG file input
    Vector3d center; //Center of quadrupole centerline at its beginning (entry)
    double alfa_q; //angle with the XZ plane, positive towards Y
    double beta_q; //angle with YZ plane, negative towards X
    double rot_q; //quad rotation around its centerline (that is defined by alpha, beta). If rot=PI/2, it changes the quad from focusing to defocusing and vica versa. The rotation is maintained along a curved centerline.
    double K_q; //Quad strength in T*m
    double L_q; //Quad centerline length (for both straight and curved)
	Vector3d magnetic_offset; // additional offset that needs to be applied for a combined function magnet    
    
    //Calculated
    Vector3d direction; //Centerline beginning, calculated from alfa_q and beta_q
    Vector3d curve_center; //Centerpoint of curved dipole rotation, calculated
    Vector3d curve_rho_at_entrance; //From center to curve_center, parallel to initial Lorentz force
    Vector3d curve_rotation_axis; //normal vector of bending plane, curve rotates around this. Axis = Crossproduct(direction, curve_rho_at_entrance)
	double sinalfa_q,cosalfa_q,sinbeta_q,cosbeta_q,sinrot_q,cosrot_q; //Cached results (constant along straight quad's all points)

	quadType type = regular;
	Vector3d B(const Vector3d &position, const double& dL_cm); //Main function returning magnetic field at a point (input and output in global coordinates)

    Vector3d ProjectToBendingPlane(const Vector3d& position); //projects a 3D point to the curved quadrupole's bending plane
    double GetBendAngle(const Vector3d& position); //In case of a curved quadrupole, gives the angle of rotation around curve_rotation_axis. Expects a position on the bending plane
    Vector3d GetCurvePosition(const double& bendAngle); //Gives a curved quad's centerline location at a bend angle
    Vector3d GetLocalTangent(const double& bendAngle); //Gives the curved quad's local tangent at a bend angle
    Vector3d GetLocalRho(const double& bendAngle); //Gives the curved quad's local rho (pointing to the center) at a bend angle

    template<class Archive>
    void serialize(Archive & archive)
    {
        archive(
                CEREAL_NVP(center),
                CEREAL_NVP(alfa_q),CEREAL_NVP(beta_q),CEREAL_NVP(rot_q),
                CEREAL_NVP(K_q),CEREAL_NVP(L_q),
                CEREAL_NVP(magnetic_offset),

                CEREAL_NVP(direction),
                CEREAL_NVP(curve_center),
                CEREAL_NVP(curve_rho_at_entrance),
                CEREAL_NVP(curve_rotation_axis),
                CEREAL_NVP(sinalfa_q),
                CEREAL_NVP(cosalfa_q),
                CEREAL_NVP(sinbeta_q),
                CEREAL_NVP(cosbeta_q),
                CEREAL_NVP(sinrot_q),
                CEREAL_NVP(cosrot_q),
                CEREAL_NVP(type)
        );
    }
};

#endif