//
// Created by Pascal Baehr on 28.04.20.
//

#ifndef SYNRAD_PROJ_GEOMETRYSIMU_H
#define SYNRAD_PROJ_GEOMETRYSIMU_H

#include <vector>
#include <Random.h>
#include "Buffer_shared.h"
#include "Region_mathonly.h"

// Local facet structure
class SubprocessFacet {
public:
    FacetProperties sh;
    FacetHitBuffer tmpCounter;

    std::vector<size_t> indices;   // Indices (Reference to geometry vertex)
    std::vector<Vector2d> vertices2; // Vertices (2D plane space, UV coordinates)
    std::vector<TextureCell> texture;

    double	 fullSizeInc; // 1/Texture FULL element area
    std::vector<double> textureCellIncrements;        // reciprocial of element area
    std::vector<bool> largeEnough; //cells that are NOT too small for autoscaling
    std::vector<DirectionCell>direction; // Direction field recording (average)
    //char     *fullElem;  // Direction field recording (only on full element) (WHY???)
    std::vector<ProfileSlice> profile;
    Histogram spectrum;

    // Temporary var (used in Intersect for collision)
    double colDist;
    double colU;
    double colV;
    double rw;
    double rh;
    double iw;
    double ih;

    // Temporary var (used in FillHit for hit recording)
    bool   isHit;
    bool   ready;         // Volatile state
    size_t    textureSize;   // Texture size (in bytes)
    size_t    profileSize;   // profile size (in bytes)
    size_t    directionSize; // direction field size (in bytes)
    size_t    spectrumSize;  // spectrum size in bytes

    size_t globalId; //Global index (to identify when superstructures are present)

    void ResetCounter();
    bool InitializeOnLoad(const size_t &id, std::vector<Region_mathonly> &regions);

    bool InitializeDirectionTexture();

    bool InitializeProfile();

    bool InitializeTexture();

    bool InitializeSpectrum(std::vector<Region_mathonly>& regions);

    bool InitializeLinkAndVolatile(const size_t & id);
};

// Local simulation structure

class AABBNODE;

class SuperStructure {
public:
    SuperStructure();
    ~SuperStructure();
    std::vector<SubprocessFacet>  facets;   // Facet handles
    AABBNODE* aabbTree; // Structure AABB tree
};

#endif //SYNRAD_PROJ_GEOMETRYSIMU_H
