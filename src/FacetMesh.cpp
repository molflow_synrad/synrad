
#include <math.h>

#include "Facet_shared.h"
#include "FacetMesh.h"
#include "GLApp/GLLabel.h"
#include "GLApp/GLToolkit.h"
#include "Helper/MathTools.h" //FormatMemory
#include "GLApp/GLMessageBox.h"

#include "SynRad.h"

FacetMesh::FacetMesh(Worker *w) : GLWindow() {

    worker = w;
    geom = w->GetGeometry();
    SetIconfiable(true);

    int wD = 320;
    int hD = 405;

    SetTitle("Facet Mesh");

    iPanel = new GLTitledPanel("Facet Dimension");
    iPanel->SetBounds(5, 10, wD - 10, 45);
    Add(iPanel);

    auto *l1 = new GLLabel("\201 length");
    l1->SetBounds(10, 30, 50, 18);
    Add(l1);

    uLength = new GLTextField(0, "");
    uLength->SetBounds(60, 30, 70, 18);
    uLength->SetEditable(false);
    Add(uLength);

    auto *l2 = new GLLabel("\202 length");
    l2->SetBounds(140, 30, 50, 18);
    Add(l2);

    vLength = new GLTextField(0, "");
    vLength->SetBounds(190, 30, 70, 18);
    vLength->SetEditable(false);
    Add(vLength);

    aPanel = new GLTitledPanel("Mesh properties");
    aPanel->SetBounds(5, 60, wD - 10, 170);
    Add(aPanel);

    // new resolution box
    enableBtn = new GLToggle(0, "Enable texture");
    aPanel->SetCompBounds(enableBtn, 5, 18, 83, 16);
    aPanel->Add(enableBtn);

    aspectRatioBtn = new GLToggle(0, "Use square cells");
    aspectRatioBtn->SetState(true);
    aPanel->SetCompBounds(aspectRatioBtn, 102, 18, 83, 16);
    aPanel->Add(aspectRatioBtn);

    boundaryBtn = new GLToggle(0, "Boundary correction");
    boundaryBtn->SetEnabled(false);
    boundaryBtn->SetTextColor(110, 110, 110);
    boundaryBtn->SetState(true);
    aPanel->SetCompBounds(boundaryBtn, 5, 39, 100, 18);
    aPanel->Add(boundaryBtn);

    l5 = new GLLabel("Resolution:");
    aPanel->SetCompBounds(l5, 5, 60, 52, 12);
    aPanel->Add(l5);

    resolutionText = new GLTextField(0, "");
    aPanel->SetCompBounds(resolutionText, 60, 60, 36, 18);
    aPanel->Add(resolutionText);

    resolutionText2 = new GLTextField(0, "");
    aPanel->SetCompBounds(resolutionText2, 104, 60, 36, 18);
    aPanel->Add(resolutionText2);

    labelXRes = new GLLabel("x");
    aPanel->SetCompBounds(labelXRes, 97, 60, 7, 12);
    aPanel->Add(labelXRes);

    perCm = new GLLabel("cells/cm");
    aPanel->SetCompBounds(perCm, 140, 60, 40, 12);
    aPanel->Add(perCm);

    lengthText = new GLTextField(0, "");
    aPanel->SetCompBounds(lengthText, 184, 60, 36, 18);
    aPanel->Add(lengthText);

    lengthText2 = new GLTextField(0, "");
    aPanel->SetCompBounds(lengthText2, 228, 60, 36, 18);
    aPanel->Add(lengthText2);

    labelXLen = new GLLabel("x");
    aPanel->SetCompBounds(labelXLen, 221, 60, 7, 12);
    aPanel->Add(labelXLen);

    perCell = new GLLabel("cm/cell");
    aPanel->SetCompBounds(perCell, 264, 60, 35, 12);
    aPanel->Add(perCell);

    // NxM texture
    nbCell = new GLLabel("Number of cells:");
    aPanel->SetCompBounds(nbCell, 25, 85, 80, 12);
    aPanel->Add(nbCell);

    cellsU = new GLTextField(0, "");
    aPanel->SetCompBounds(cellsU, 108, 82, 40, 18);
    aPanel->Add(cellsU);

    labelXCells = new GLLabel(" x ");
    aPanel->SetCompBounds(labelXCells, 151, 85, 15, 12);
    aPanel->Add(labelXCells);

    cellsV = new GLTextField(0, "");
    aPanel->SetCompBounds(cellsV, 169, 82, 40, 18);
    aPanel->Add(cellsV);

    // new resolution box
    recordAbsBtn = new GLToggle(0, "Count absorption");
    recordAbsBtn->SetState(false);
    aPanel->SetCompBounds(recordAbsBtn, 5, 129, 94, 16);
    aPanel->Add(recordAbsBtn);

    recordReflBtn = new GLToggle(0, "Count reflection");
    recordReflBtn->SetState(false);
    aPanel->SetCompBounds(recordReflBtn, 165, 108, 89, 16);
    aPanel->Add(recordReflBtn);

    recordTransBtn = new GLToggle(0, "Count transparent pass");
    recordTransBtn->SetState(false);
    aPanel->SetCompBounds(recordTransBtn, 165, 129, 120, 16);
    aPanel->Add(recordTransBtn);

    recordDirBtn = new GLToggle(0, "Record direction vectors");
    recordDirBtn->SetState(false);
    aPanel->SetCompBounds(recordDirBtn, 165, 150, 125, 16);
    aPanel->Add(recordDirBtn);

    vPanel = new GLTitledPanel("View Settings");
    vPanel->SetBounds(5, 235, wD - 10, 45);
    Add(vPanel);

    showTexture = new GLToggle(0, "Show texture");
    showTexture->SetBounds(10, 255, 55, 18);
    showTexture->SetState(true);
    Add(showTexture);

    showVolume = new GLToggle(0, "Show volume");
    showVolume->SetBounds(100, 255, 55, 18);
    showVolume->SetState(true);
    showVolume->SetVisible(true); //not working yet
    Add(showVolume);

    quickApply = new GLButton(0, "Apply View");  //Apply View Settings without stopping the simulation
    quickApply->SetBounds(190, 255, 72, 19);
    Add(quickApply);

    GLTitledPanel *mPanel = new GLTitledPanel("Memory/Cell");
    mPanel->SetBounds(5, 285, wD - 10, 72);
    Add(mPanel);

    GLLabel *l7 = new GLLabel("Memory estimate:");
    l7->SetBounds(10, 305, 70, 18);
    Add(l7);

    ramText = new GLTextField(0, "");
    ramText->SetBounds(130, 305, 100, 18);
    Add(ramText);

    GLLabel *l8 = new GLLabel("Cells:");
    l8->SetBounds(10, 330, 70, 18);
    Add(l8);

    cellText = new GLTextField(0, "");
    cellText->SetBounds(130, 330, 100, 18);
    Add(cellText);

    applyButton = new GLButton(0, "Apply mesh");
    applyButton->SetBounds(wD - 200, hD - 43, 95, 21);
    Add(applyButton);

    cancelButton = new GLButton(0, "Cancel");
    cancelButton->SetBounds(wD - 100, hD - 43, 95, 21);
    Add(cancelButton);

    UpdateSquaredCells(true);
    cellText->SetEditable(false);
    ramText->SetEditable(false);
    EditFacet(std::vector<size_t>());

    // Center dialog
    int wS, hS;
    GLToolkit::GetScreenSize(&wS, &hS);
    int xD = (wS - wD) / 2;
    int yD = (hS - hD) / 2;
    SetBounds(xD, yD, wD, hD);

    RestoreDeviceObjects();

}

void FacetMesh::UpdateSquaredCells(int aspectState) {
    int x, y, w, h;
    if (aspectState) {
        resolutionText2->SetText(resolutionText->GetText());
        lengthText2->SetText(lengthText->GetText());
        resolutionText->GetBounds(&x, &y, &w, &h);
        resolutionText->SetBounds(x, y, 80, 18);
        lengthText->GetBounds(&x, &y, &w, &h);
        lengthText->SetBounds(x, y, 80, 18);
    } else {
        resolutionText->GetBounds(&x, &y, &w, &h);
        resolutionText->SetBounds(x, y, 36, 18);
        lengthText->GetBounds(&x, &y, &w, &h);
        lengthText->SetBounds(x, y, 36, 18);
    }

    auto enableState = enableBtn->GetState();
    auto sel = geom->GetSelectedFacets();
    if (!sel.empty()) {
        resolutionText2->SetEditable(!aspectState);
        lengthText2->SetEditable(!aspectState);
    }
    if (sel.size() > 1) {
        cellsU->SetEditable(false);
        cellsV->SetEditable(false);
        // non mixed, squared are editable, mixed state is not
        lengthText->SetEditable(aspectState < 2 && enableState < 2);
        resolutionText->SetEditable(aspectState < 2 && enableState < 2);
    } else if (sel.size() == 1) {
        cellsU->SetEditable(true);
        cellsV->SetEditable(!aspectState);
    }

    resolutionText2->SetVisible(!aspectState);
    lengthText2->SetVisible(!aspectState);
    labelXLen->SetVisible(!aspectState);
    labelXRes->SetVisible(!aspectState);

    // Sync negative states to 2nd dimension
    if (!lengthText->IsEditable()) lengthText2->SetEditable(false);
    if (!resolutionText->IsEditable()) resolutionText2->SetEditable(false);
}

void FacetMesh::UpdateSize() {

    char tmp[64];

    if (enableBtn->GetState()) {

        size_t ram = 0;
        size_t cell = 0;
        size_t nbFacet = geom->GetNbFacet();

        for (size_t i = 0; i < nbFacet; i++) {
            Facet *f = geom->GetFacet(i);
            auto nbCells = f->GetNbCell();
            cell += (size_t) (nbCells.first * nbCells.second);
            ram += (size_t) f->GetTexRamSize();

        }


        ramText->SetText(FormatMemoryLL(ram));
        sprintf(tmp, "%zd", cell);
        cellText->SetText(tmp);

    } else {

        ramText->SetText("0 bytes");
        cellText->SetText("0");

    }

}

void FacetMesh::UpdateSizeForRatio() {
    if (!geom->IsLoaded()) return;

    double ratioU = 0.0;
    double ratioV = 0.0;
    char tmp[64];
    bool boundMap = boundaryBtn->GetState();
    bool recordDir = recordDirBtn->GetState();

    if (!enableBtn->GetState()) {
        ramText->SetText(FormatMemory(0));
        cellText->SetText("0");
        return;
    }

    if (!resolutionText->GetNumber(&ratioU) || !resolutionText2->GetNumber(&ratioV)) {
        ramText->SetText("");
        cellText->SetText("");
        return;
    }

    size_t ram = 0;
    size_t cell = 0;
    size_t nbFacet = geom->GetNbFacet();

    for (size_t i = 0; i < nbFacet; i++) {
        Facet *f = geom->GetFacet(i);
        if (f->selected) {
            auto nbCells = f->GetNbCellForRatio(ratioU, ratioV);
            cell += (size_t) (nbCells.first * nbCells.second);
            ram += f->GetTexRamSizeForRatio(ratioU);
        } else {
            auto nbCells = f->GetNbCell();
            cell += (size_t) (nbCells.first * nbCells.second);
            ram += (size_t) f->GetTexRamSize();
        }
    }


    ramText->SetText(FormatMemoryLL(ram));
    sprintf(tmp, "%d", (int) cell);
    cellText->SetText(tmp);

}

void FacetMesh::EditFacet(std::vector<size_t> selection) {

    char tmp[128];
    double maxU = 0.0;
    double maxV = 0.0;
    double minU = 1.0e100;
    double minV = 1.0e100;

    bool somethingSelected = !selection.empty();
    bool multiSelect = selection.size() > 1;


    size_t nbS = 0;
    size_t nbF = geom->GetNbFacet();
    size_t sel = 0;

    enableBtn->SetEnabled(somethingSelected);
    boundaryBtn->SetEnabled(somethingSelected);
    recordAbsBtn->SetEnabled(somethingSelected);
    recordReflBtn->SetEnabled(somethingSelected);
    recordTransBtn->SetEnabled(somethingSelected);
    recordDirBtn->SetEnabled(somethingSelected);
    showTexture->SetEnabled(somethingSelected);
    showVolume->SetEnabled(somethingSelected);

    resolutionText->SetEditable(somethingSelected);
    lengthText->SetEditable(somethingSelected);
    resolutionText2->SetEditable(somethingSelected);
    lengthText2->SetEditable(somethingSelected);
    cellsU->SetEditable(!multiSelect && somethingSelected);
    cellsV->SetEditable(!multiSelect && somethingSelected && !aspectRatioBtn->GetState());

    if (!geom->IsLoaded()) return;


    if (!somethingSelected) { //Empty selection, clear
        enableBtn->SetState(0);
        resolutionText->SetText("");
        lengthText->SetText("");
        resolutionText2->SetText("");
        lengthText2->SetText("");
        cellsU->SetText("");
        cellsV->SetText("");
        recordAbsBtn->SetState(0);
        recordReflBtn->SetState(0);
        recordTransBtn->SetState(0);
        recordDirBtn->SetState(0);
        showTexture->SetState(0);
        showVolume->SetState(0);
        return;
    }

    Facet *f0 = geom->GetFacet(selection[0]);

    bool allEnabled = true;
    bool allBound = true;
    bool ratioE = true;
    bool ratioVE = true;
    bool allCountAbs = true;
    bool allCountRefl = true;
    bool allCountTrans = true;
    bool allCountDir = true;
    bool allTexVisible = true;
    bool allVolVisible = true;
    uint32_t squaredCellsE = std::abs(f0->tRatioU - f0->tRatioV) < 1E-8 ? 1 : 2; // 1==squared cells, 2==non-squared cells, 0==mixed state



    {
        double nU = f0->sh.U.Norme();
        double nV = f0->sh.V.Norme();
        maxU = Max(maxU, nU);
        maxV = Max(maxV, nV);
        minU = Min(minU, nU);
        minV = Min(minV, nV);
    }
    for (size_t i = 1; i < selection.size(); i++) {
        Facet *f = geom->GetFacet(selection[i]);
        double nU = f->sh.U.Norme();
        double nV = f->sh.V.Norme();
        maxU = Max(maxU, nU);
        maxV = Max(maxV, nV);
        minU = Min(minU, nU);
        minV = Min(minV, nV);
        sel = i;
        allEnabled = allEnabled && (f0->sh.isTextured == f->sh.isTextured);
        allCountAbs = allCountAbs && f0->sh.countAbs == f->sh.countAbs;
        allCountRefl = allCountRefl && f0->sh.countRefl == f->sh.countRefl;
        allCountTrans = allCountTrans && f0->sh.countTrans == f->sh.countTrans;
        allCountDir = allCountDir && f0->sh.countDirection == f->sh.countDirection;
        allBound = allBound && (f->cellPropertiesIds != nullptr);
        allTexVisible = allTexVisible && f0->textureVisible == f->textureVisible;
        allVolVisible = allVolVisible && f0->volumeVisible == f->volumeVisible;
        squaredCellsE = squaredCellsE & (std::abs(f->tRatioU - f->tRatioV) < 1E-8 ? 1u : 2u);
        ratioE = ratioE && std::abs(f0->tRatioU - f->tRatioU) < 1E-8;
        ratioVE = ratioVE && std::abs(f0->tRatioV - f->tRatioV) < 1E-8;
        nbS++;
    }

    if (nbS == 1) {
        Facet *f = geom->GetFacet(sel);
        sprintf(tmp, "Facet Info (#%zd)", sel + 1);
        iPanel->SetTitle(tmp);
        sprintf(tmp, "%g", maxU);
        uLength->SetText(tmp);
        sprintf(tmp, "%g", maxV);
        vLength->SetText(tmp);
    } else {
        sprintf(tmp, "Facet Info (%zd selected)", nbS);
        iPanel->SetTitle(tmp);
        sprintf(tmp, "%g (MAX)", maxU);
        uLength->SetText(tmp);
        sprintf(tmp, "%g (MAX)", maxV);
        vLength->SetText(tmp);
    }

    enableBtn->AllowMixedState(!allEnabled);
    enableBtn->SetState(allEnabled ? f0->sh.isTextured : 2);
    recordAbsBtn->AllowMixedState(!allCountAbs);
    recordAbsBtn->SetState(allCountAbs ? f0->sh.countAbs : 2);
    recordReflBtn->AllowMixedState(!allCountRefl);
    recordReflBtn->SetState(allCountRefl ? f0->sh.countRefl : 2);
    recordTransBtn->AllowMixedState(!allCountTrans);
    recordTransBtn->SetState(allCountTrans ? f0->sh.countTrans : 2);
    recordDirBtn->AllowMixedState(!allCountDir);
    recordDirBtn->SetState(allCountDir ? f0->sh.countDirection : 2);
    showTexture->AllowMixedState(!allTexVisible);
    showTexture->SetState(allTexVisible ? f0->textureVisible : 2);
    showVolume->AllowMixedState(!allVolVisible);
    showVolume->SetState(allVolVisible ? f0->volumeVisible : 2);

    boundaryBtn->AllowMixedState(true);
    boundaryBtn->SetState(true);

    if (allEnabled) {
        if (f0->sh.isTextured) { //All facets have textures
            if (ratioE) { //All facets have textures with same resolution
                resolutionText->SetText(f0->tRatioU);
                lengthText->SetText(1.0 / f0->tRatioU);
                cellsU->SetText(allEnabled ? "..." : "");
                cellsV->SetText(allEnabled ? "..." : "");
            }
            if (ratioVE){
                resolutionText2->SetText(f0->tRatioV);
                lengthText2->SetText(1.0 / f0->tRatioV);
                cellsU->SetText(allEnabled ? "..." : "");
                cellsV->SetText(allEnabled ? "..." : "");
            }
            if(!(ratioE && ratioVE)) { //Mixed resolution
                resolutionText->SetText(allEnabled ? "..." : "");
                lengthText->SetText(allEnabled ? "..." : "");
                resolutionText2->SetText(allEnabled ? "..." : "");
                lengthText2->SetText(allEnabled ? "..." : "");
                cellsU->SetText(allEnabled ? "..." : "");
                cellsV->SetText(allEnabled ? "..." : "");
            }
            if(!squaredCellsE){ // mixed state for squared or non-squared cells
                resolutionText->SetEditable(false);
                lengthText->SetEditable(false);
                resolutionText2->SetEditable(false);
                lengthText2->SetEditable(false);
                cellsU->SetEditable(false);
                cellsV->SetEditable(false);
            }
            // Allow for cell number input only when one facet is selected
            if(!multiSelect){
                auto nbCells = f0->GetNbCellForRatio(f0->tRatioU,f0->tRatioV);
                cellsU->SetText(allEnabled ? std::to_string(nbCells.first) : "");
                cellsV->SetText(allEnabled ? std::to_string(nbCells.second) : "");
                auto aspectState = IsZero(std::abs(f0->tRatioU-f0->tRatioV));
                UpdateSquaredCells(aspectState);
                cellsU->SetEditable(true);
                cellsV->SetEditable(!aspectState);
            }
            else{
                cellsU->SetEditable(false);
                cellsV->SetEditable(false);
            }
        }
        else { //None of the facets have textures
            aspectRatioBtn->SetState(true);
            resolutionText->SetText("");
            lengthText->SetText("");
            resolutionText2->SetText("");
            lengthText2->SetText("");
            cellsU->SetText("");
            cellsV->SetText("");
            cellsU->SetText("");
            UpdateSquaredCells(aspectRatioBtn->GetState());
        }
    }
    else { //Mixed state
        resolutionText->SetEditable(false);
        lengthText->SetEditable(false);
        resolutionText2->SetEditable(false);
        lengthText2->SetEditable(false);
        cellsU->SetEditable(false);
        cellsV->SetEditable(false);

        lengthText->SetText("");
        resolutionText2->SetText("");
        lengthText2->SetText("");
        cellsU->SetText("");
        cellsV->SetText("");
    }

    UpdateSize();
    DoModal();

}

bool FacetMesh::Apply(bool force) {
    extern GLApplication *theApp;
    SynRad *mApp = (SynRad *) theApp;
    bool boundMap = boundaryBtn->GetState();
    std::vector<size_t> selectedFacets = geom->GetSelectedFacets();
    size_t nbPerformed = 0;

    // Auto resolution
    double ratioU = 0.0;
    double ratioV = 0.0;
    bool doRatio = false;
    if (enableBtn->GetState()) {

        // Check counting mode
        if (!recordAbsBtn->GetState() &&
            !recordReflBtn->GetState() && !recordTransBtn->GetState() &&
            !recordDirBtn->GetState()) {
            GLMessageBox::Display("Please select counting mode", "Error", GLDLG_OK, GLDLG_ICONERROR);
            return false;
        }


        // Resolution
        if (resolutionText->GetNumber(&ratioU) && resolutionText2->GetNumber(&ratioV) && ratioU >= 0.0 &&
            ratioV >= 0.0) {
            //Got a valid number
            doRatio = true;
        } else if (resolutionText->GetText() != "..." ||
                   resolutionText2->GetText() != "...") { //Not in mixed "..." state
            GLMessageBox::Display("Invalid texture resolution\nMust be a non-negative number", "Error", GLDLG_OK,
                                  GLDLG_ICONERROR);
            return false;
        } else {
            //Mixed state: leave doRatio as false
        }
    }

    if (!mApp->AskToReset(worker)) return false;
    progressDlg = new GLProgress("Applying mesh settings", "Please wait");
    progressDlg->SetVisible(true);
    progressDlg->SetProgress(0.0);
    int count = 0;
    for (auto &sel : selectedFacets) {
        Facet *f = geom->GetFacet(sel);

        bool hadAnyTexture = f->sh.countAbs || f->sh.countRefl || f->sh.countTrans || f->sh.countDirection;
        bool hadDirCount = f->sh.countDirection;

        if (enableBtn->GetState() == 0 || (doRatio && (ratioU == 0.0 || ratioV == 0.0))) {
            //Let the user disable textures with the main switch or by typing 0 as resolution
            f->sh.countAbs = f->sh.countRefl = f->sh.countTrans = f->sh.countDirection = false;
        } else {
            if (recordAbsBtn->GetState() < 2) f->sh.countAbs = recordAbsBtn->GetState();
            if (recordReflBtn->GetState() < 2) f->sh.countRefl = recordReflBtn->GetState();
            if (recordTransBtn->GetState() < 2) f->sh.countTrans = recordTransBtn->GetState();
            if (recordDirBtn->GetState() < 2) f->sh.countDirection = recordDirBtn->GetState();
        }

        if (aspectRatioBtn->GetState() == 1 && !IsZero(f->tRatioU - f->tRatioV)) {
            f->tRatioV = f->tRatioU;
        }

        f->textureVisible = showTexture->GetState();
        f->volumeVisible = showVolume->GetState();

        bool hasAnyTexture = f->sh.countAbs || f->sh.countRefl || f->sh.countTrans || f->sh.countDirection;

        try {
            bool needsRemeshing = force || (hadAnyTexture != hasAnyTexture) || (hadDirCount != f->sh.countDirection)
                                  || (doRatio && ((!IsZero(geom->GetFacet(sel)->tRatioU - ratioU)) ||
                                                  (!IsZero(geom->GetFacet(sel)->tRatioV - ratioV))));
            if (needsRemeshing)
                geom->SetFacetTexture(sel, hasAnyTexture ? (doRatio ? ratioU : f->tRatioU) : 0.0,
                                      hasAnyTexture ? (doRatio ? ratioV : f->tRatioV) : 0.0,
                                      hasAnyTexture ? boundMap : false);
        } catch (Error &e) {
            char errMsg[512];
            sprintf(errMsg, "Error setting textures:\n%s", e.what());
            GLMessageBox::Display(errMsg, "Error", GLDLG_OK, GLDLG_ICONERROR);
            progressDlg->SetVisible(false);
            SAFE_DELETE(progressDlg);
            return false;
        }
        catch (...) {
            GLMessageBox::Display("Unexpected error while setting textures", "Error", GLDLG_OK, GLDLG_ICONWARNING);
            progressDlg->SetVisible(false);
            SAFE_DELETE(progressDlg);
            return false;
        }
        nbPerformed++;
        progressDlg->SetProgress((double) nbPerformed / (double) selectedFacets.size());
    }

    // Update state in case of change
    UpdateSquaredCells(aspectRatioBtn->GetState());

    // Send to sub process
    try {
        worker->Reload();

    } catch (Error &e) {
        GLMessageBox::Display((char *) e.what(), "Error reloading worker", GLDLG_OK, GLDLG_ICONERROR);
    }
    if (progressDlg) progressDlg->SetVisible(false);
    SAFE_DELETE(progressDlg);
    return true;

}

void FacetMesh::ApplyDrawSettings() {
    //Apply view settings without stopping the simulation

    double nbSelected = (double) geom->GetNbSelectedFacets();
    double nbPerformed = 0.0;

    for (int i = 0; i < geom->GetNbFacet(); i++) {

        Facet *f = geom->GetFacet(i);
        if (f->selected) {

            if (showTexture->GetState() < 2) f->textureVisible = showTexture->GetState();
            if (showVolume->GetState() < 2) f->volumeVisible = showVolume->GetState();

            nbPerformed += 1.0;
            progressDlg->SetProgress(nbPerformed / nbSelected);
        }

    }
    geom->BuildGLList();
}

void FacetMesh::UpdateToggle(GLComponent *src) {

    if (src == enableBtn) {
        UpdateSquaredCells(aspectRatioBtn->GetState());
    } else if (src == aspectRatioBtn) {
        auto aspectState = aspectRatioBtn->GetState();
        UpdateSquaredCells(aspectState);
    } else if (src == recordAbsBtn || src == recordReflBtn || src == recordTransBtn || src == recordDirBtn) {
        enableBtn->SetState(true);
        boundaryBtn->SetState(true);
    }
    UpdateSizeForRatio();
}

void FacetMesh::ProcessMessage(GLComponent *src, int message) {

    switch (message) {
        case MSG_BUTTON:
            if (src == cancelButton) {

                GLWindow::ProcessMessage(nullptr, MSG_CLOSE);

            } else if (src == applyButton) {

                //if (worker->running) worker->Stop_Public();
                if (Apply(true))
                    GLWindow::ProcessMessage(nullptr, MSG_CLOSE);

            } else if (src == quickApply) {

                progressDlg = new GLProgress("Applying view settings", "Please wait");
                progressDlg->SetVisible(true);
                progressDlg->SetProgress(0.5);

                ApplyDrawSettings();
                GLWindow::ProcessMessage(nullptr, MSG_CLOSE);

                progressDlg->SetVisible(false);
                SAFE_DELETE(progressDlg);

            }
            break;


        case MSG_TEXT_UPD:
            enableBtn->SetState(true);
            //mApp->facetApplyBtn->SetEnabled(true);
            if (src == resolutionText) {
                auto aspectState = aspectRatioBtn->GetState();

                enableBtn->SetState(true);
                double res;
                double resV;
                // Fetch and test for a valid number
                if (resolutionText->GetNumber(&res) && res != 0.0) {
                    lengthText->SetText(1.0 / res);
                    cellsU->SetText(0);
                }
                else
                    lengthText->SetText("");
                if (aspectState){
                    resolutionText2->SetText(resolutionText->GetText());
                    lengthText2->SetText(lengthText->GetText());
                }

                // Calculate and display the number of actualy texels for each direction
                auto selFacets = geom->GetSelectedFacets();
                if(selFacets.size() == 1) {
                    resolutionText2->GetNumber(&resV);
                    auto nbCells = geom->GetFacet(selFacets.front())->GetNbCellForRatio(res, resV);
                    cellsU->SetText(nbCells.first);
                    cellsV->SetText(nbCells.second);
                }
                else{ // mixed state
                    cellsU->SetText("...");
                    cellsV->SetText("...");
                }
                UpdateSizeForRatio();
            }
            else if (src == resolutionText2) {
                enableBtn->SetState(true);
                double res;
                // Fetch and test for a valid number
                if (resolutionText2->GetNumber(&res) && res != 0.0)
                    lengthText2->SetText(1.0 / res);
                else
                    lengthText2->SetText("");

                // Calculate and display the number of actualy texels for each direction
                auto selFacets = geom->GetSelectedFacets();
                if(selFacets.size() == 1) {
                    double resU;
                    resolutionText->GetNumber(&resU);
                    auto nbCells = geom->GetFacet(selFacets.front())->GetNbCellForRatio(resU, res);
                    cellsU->SetText(nbCells.first);
                    cellsV->SetText(nbCells.second);
                }
                else{ // mixed state
                    cellsU->SetText("...");
                    cellsV->SetText("...");
                }
                UpdateSizeForRatio();
            }
            else if (src == lengthText) {
                auto aspectState = aspectRatioBtn->GetState();

                enableBtn->SetState(true);
                double length;
                if (lengthText->GetNumber(&length) && length != 0.0) {
                    resolutionText->SetText(1.0 / length);
                    UpdateSizeForRatio();
                }
                else
                    resolutionText->SetText("");

                if(aspectState){
                    lengthText2->SetText(lengthText->GetText());
                    resolutionText2->SetText(resolutionText->GetText());
                }

                // Calculate and display the number of actualy texels for each direction
                auto selFacets = geom->GetSelectedFacets();
                if(selFacets.size() == 1) {
                    double resU;
                    double resV;
                    resolutionText->GetNumber(&resU);
                    resolutionText2->GetNumber(&resV);
                    auto nbCells = geom->GetFacet(selFacets.front())->GetNbCellForRatio(resU, resV);
                    cellsU->SetText(nbCells.first);
                    cellsV->SetText(nbCells.second);
                }
                else{ // mixed state
                    cellsU->SetText("...");
                    cellsV->SetText("...");
                }
                UpdateSizeForRatio();
            }
            else if (src == lengthText2) {
                enableBtn->SetState(true);
                double length;
                if (lengthText2->GetNumber(&length) && length != 0.0) {
                    resolutionText2->SetText(1.0 / length);}
                else
                    resolutionText2->SetText("");

                // Calculate and display the number of actualy texels for each direction
                auto selFacets = geom->GetSelectedFacets();
                if(selFacets.size() == 1) {
                    double resU;
                    double resV;
                    resolutionText->GetNumber(&resU);
                    resolutionText2->GetNumber(&resV);
                    auto nbCells = geom->GetFacet(selFacets.front())->GetNbCellForRatio(resU, resV);
                    cellsU->SetText(nbCells.first);
                    cellsV->SetText(nbCells.second);
                }
                else{ // mixed state
                    cellsU->SetText("...");
                    cellsV->SetText("...");
                }
                UpdateSizeForRatio();
            }
            else if (src == cellsU || src == cellsV) {
                enableBtn->SetState(true);
                int nbCellsU = 0;
                int nbCellsV = 0;
                if (cellsU->GetNumberInt(&nbCellsU) && nbCellsU != 0) {
                    if(aspectRatioBtn->GetState() == 1) {
                        auto selected = geom->GetSelectedFacets();
                        if(selected.size() == 1) {
                            //apply same ratio for both cells
                            auto ratio = GetRatioForNbCell(nbCellsU, nbCellsU);
                            Facet* fac = geom->GetFacet(selected.front());
                            auto nbCells = fac->GetNbCellForRatio(ratio.first,ratio.first);
                            cellsV->SetText(nbCells.second);
                        }
                    }
                    if (cellsV->GetNumberInt(&nbCellsV) && nbCellsV != 0) {
                        auto ratio = GetRatioForNbCell(nbCellsU, nbCellsV);

                        resolutionText->SetText(ratio.first);
                        resolutionText2->SetText(ratio.second);
                        if (ratio.first != 0.0)
                            lengthText->SetText(1.0 / ratio.first);
                        if (ratio.second != 0.0)
                            lengthText2->SetText(1.0 / ratio.second);

                        UpdateSizeForRatio();
                    }
                }
            }

            break;


        case MSG_TOGGLE:
            UpdateToggle(src);
            break;

        default:
            break;
    }

    GLWindow::ProcessMessage(src, message);
}

/**
* \brief Get number of cells calculated with a size ratio
* \param ratioU ratio in U direction used for size conversion
* \param ratioV ratio in V direction used for size conversion
* \return number of texture cells
*/
std::pair<double,double> FacetMesh::GetRatioForNbCell(size_t nbCellsU, size_t nbCellsV) {

    double ratioU = 0.0;
    double ratioV = 0.0;
    auto selFacets = geom->GetSelectedFacets();

    if(selFacets.size() == 1) {
        for (auto &sel : selFacets) {
            Facet *f = geom->GetFacet(sel);
            if (f->selected) {
                double nU = f->sh.U.Norme();
                double nV = f->sh.V.Norme();

                if (nU != 0.0)
                    ratioU = (double) nbCellsU / nU;
                if (nV != 0.0)
                    ratioV = (double) nbCellsV / nV;
            }
        }
    }
    return std::make_pair(ratioU,ratioV);
}
