
#include "GLApp/GLWindow.h"
#include "GLApp/GLChart/GLChart.h"
#include "GLApp/GLLabel.h"
#include "GLApp/GLCombo.h"
#include "GLApp/GLButton.h"
#include "GLApp/GLParser.h"
#include "GLApp/GLTextField.h"
#include "Worker.h"
#include "Geometry_shared.h"

#ifndef _PROFILEPLOTTERH_
#define _PROFILEPLOTTERH_

class ProfilePlotter : public GLWindow {

public:

  // Construction
  ProfilePlotter();

  // Component method
  void Display(Worker *w);
  void Refresh();
  void Update(float appTime,bool force=false);
  void Reset();

  // Implementation
  void ProcessMessage(GLComponent *src,int message);
  void SetBounds(int x,int y,int w,int h);
  void addView(int facet);
  std::vector<int> GetViews();
  void SetViews(std::vector<int> views,int mode);
  bool IsLogScaled();
  void SetLogScaled(bool logScale);
  void SetWorker(Worker *w);

private:

  void addView(int facet,int mode);
  void remView(int facet,int mode);
  void refreshViews();
  void plot();

  Worker      *worker;
  GLButton    *dismissButton;
  GLChart     *chart;
  GLCombo     *profCombo;
  GLToggle    *logToggle;
  GLToggle    *normToggle;
  GLButton    *selButton;
  GLButton    *addButton;
  GLButton    *removeButton;
  GLButton    *resetButton;
  GLTextField *formulaText;
  GLButton    *formulaBtn;

  GLDataView  *views[32];
  GLColor    *colors[8];

  int          nbColors;
  int          nbView;
  float        lastUpdate;

};

#endif /* _PROFILEPLOTTERH_ */
