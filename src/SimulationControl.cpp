
#if defined(WIN32) || defined(_WIN32) || defined(WIN64) || defined(_WIN64)
#define NOMINMAX
//#include <windows.h> // For GetTickCount()
#include <process.h> // For _getpid()
#else
//#include <time.h>
#include <sys/time.h>
#endif

#include <cmath>
#include <cstdio>
#include <vector>
#include <sstream>
#include "Simulation.h"
#include "IntersectAABB_shared.h"
#include "Random.h"
#include "SynradTypes.h" //Histogram
#include "ProcessControl.h"

//#include "Tools.h"
#include <cereal/types/utility.hpp>
#include <cereal/archives/binary.hpp>
#include <cereal/types/vector.hpp>
#include <cereal/types/string.hpp>
#include <cereal/archives/json.hpp>

//extern void SetErrorSub(const char *message);

// Global handles

//extern Simulation *sHandle;

/*void InitSimulation() {

	// Global handle allocation
	sHandle = new Simulation();
	InitTick();
}*/

int Simulation::SanityCheckGeom() {

    if (regions.empty()) {
        //SetErrorSub("No regions");
        std::cerr << "No regions"<<std::endl;
        return 1;
    }

    //Check for invalid material (ID==9) passed from the interface
    //It is valid to load invalid materials (to extract textures, etc.) but not to launch the simulation
    for(auto& s : structures) {
        for (auto& f : s.facets) {
            if (f.sh.reflectType == 9) {
                char tmp[32];
                sprintf(tmp, "Invalid material on Facet %zd.", f.globalId + 1);
                //SetErrorSub(tmp);
                std::cerr << tmp<<std::endl;

                return 1;
            }
        }
    }
    
    return 0; // all ok
}

bool Simulation::LoadSimulation(Dataport *loader, char *loadStatus) {

    double t0 = GetTick();

    //SetState(PROCESS_STARTING, "Clearing previous simulation");
    strncpy(loadStatus, "Clearing previous simulation", 127);

    ClearSimulation();

    //SetState(PROCESS_STARTING, "Loading simulation");
    strncpy(loadStatus, "Loading simulation", 127);

	{

		std::string inputString(loader->size,'\0');
		BYTE* buffer = (BYTE*)loader->buff;
		std::copy(buffer, buffer + loader->size, inputString.begin());
		std::stringstream inputStream;
		inputStream << inputString;
		cereal::BinaryInputArchive inputArchive(inputStream);

		//Worker params
		inputArchive(wp);
		//regions.resize(wp.nbRegion); //Create structures
		inputArchive(ontheflyParams);
		inputArchive(regions); // mathonly
		inputArchive(materials);
		inputArchive(psi_distro);
		inputArchive(chi_distros);
		inputArchive(parallel_polarization);

		//Geometry
		inputArchive(sh);
		inputArchive(vertices3);

		// Prepare super structure
		structures.resize(sh.nbSuper); //Create structures

		//SetState(PROCESS_STARTING, ("Loading facets"));

		//Facets
		for (size_t i = 0; i < sh.nbFacet; i++) { //Necessary because facets is not (yet) a vector in the interface
            {
                char tmp[128];
                sprintf(tmp,"Loading facet #%u / %u", i, sh.nbFacet);
                strncpy(loadStatus, tmp, 127);
            }
            SubprocessFacet f;
			inputArchive(f.sh);
			inputArchive(f.indices);
			inputArchive(f.vertices2);
			inputArchive(f.textureCellIncrements);

			//Some initialization
			if (!f.InitializeOnLoad(i, regions)) return false;
            // Increase size counters
            dirTotalSize += f.directionSize;
            profTotalSize += f.profileSize;
            textTotalSize += f.textureSize;
            spectrumTotalSize += f.spectrumSize;

            if (f.sh.superIdx == -1) { //Facet in all structures
				for (auto& s : structures) {
				    s.facets.push_back(f);
				}
			}
            else {
				structures[f.sh.superIdx].facets.push_back(f); //Assign to structure
			}
		}
    }//inputarchive goes out of scope, file released

    wp.nbTrajPoints = 0;

    // dont check at launch
    /*if (sh.nbSuper <= 0) {
        //ReleaseDataport(loader);
        SetErrorSub("No structures");
        return false;
    }*/

    try {
        //copy trajectory points
        for (auto& reg : regions) {
            reg.Points.resize(reg.params.nbPointsToCopy);
            wp.nbTrajPoints += reg.params.nbPointsToCopy;
        }

        //copy distribution points
        nbDistrPoints_BXY = 0;
        for (auto& reg : regions) {
            reg.Bx_distr.Resize((size_t)reg.params.nbDistr_MAG.x);
            reg.By_distr.Resize((size_t)reg.params.nbDistr_MAG.y);
            reg.Bz_distr.Resize((size_t)reg.params.nbDistr_MAG.z);
            reg.latticeFunctions.Resize(0);

            nbDistrPoints_BXY += reg.params.nbDistr_BXY;
        }
    }
    catch (...) {
        //SetErrorSub("Error loading regions");
        std::cerr << "Error loading regions" <<std::endl;

        return false;
    }
    ComputeSourceArea();

    //Reserve particle log
    if (ontheflyParams.enableLogging)
        tmpParticleLog.reserve(ontheflyParams.logLimit / ontheflyParams.nbProcess);

	// Build all AABBTrees
	size_t maxDepth = 0;
	for (auto& s : structures) {
		std::vector<SubprocessFacet*> facetPointers; facetPointers.reserve(s.facets.size());
		for (auto& f : s.facets) {
			facetPointers.push_back(&f);
		}
		s.aabbTree = BuildAABBTree(facetPointers, 0, maxDepth);
	}

	// Initialise simulation

	loadOK = true;
	double t1 = GetTick();
	printf("  Load %s successful\n", sh.name.c_str());
	printf("  Geometry: %zd vertex %zd facets\n", vertices3.size(), sh.nbFacet);
	printf("  Region: %zd regions\n", wp.nbRegion);
	printf("  Trajectory points: %zd points\n", wp.nbTrajPoints);
	printf("  Geom size: %d bytes\n", /*(size_t)(buffer - bufferStart)*/0);
	printf("  Number of stucture: %zd\n", sh.nbSuper);
	printf("  Global Hit: %zd bytes\n", sizeof(GlobalHitBuffer));
	printf("  Facet Hit : %zd bytes\n", sh.nbFacet * sizeof(FacetHitBuffer));
	printf("  Texture   : %zd bytes\n", textTotalSize);
	printf("  Profile   : %zd bytes\n", profTotalSize);
	printf("  Direction : %zd bytes\n", dirTotalSize);
	printf("  Spectrum  : %zd bytes\n", spectrumTotalSize);
	printf("  Total     : %zd bytes\n", GetHitsSize());
    printf("  Seed: %lu\n", randomGenerator.GetSeed());
	printf("  Loading time: %.3f ms\n", (t1 - t0)*1000.0);
	return true;

}

bool Simulation::UpdateOntheflySimuParams(Dataport *loader) {
	// Connect the dataport
	if (!AccessDataportTimed(loader, 2000)) {
		//SetErrorSub("Failed to connect to loader DP");
		std::cerr << "Failed to connect to loader DP" << std::endl;
		return false;
	}

    std::string inputString(loader->size,'\0');
    BYTE* buffer = (BYTE*)loader->buff;
    std::copy(buffer, buffer + loader->size, inputString.begin());
    std::stringstream inputStream;
    inputStream << inputString;
    cereal::BinaryInputArchive inputArchive(inputStream);

    inputArchive(ontheflyParams);
    for (size_t i = 0; i < wp.nbRegion; i++) {
        inputArchive(regions[i].params.showPhotons);
    }
    ReleaseDataport(loader);

	return true;
}

bool Simulation::UpdateHits(Dataport *dpHit, Dataport* dpLog, int prIdx, DWORD timeout) {
    bool lastHitUpdateOK = false;
    lastHitUpdateOK = UpdateMCHits(dpHit, prIdx, timeout);
    if (lastHitUpdateOK && dpLog) UpdateLog(dpLog, timeout);

    return lastHitUpdateOK;
}

size_t Simulation::GetHitsSize() {
	return textTotalSize + profTotalSize + dirTotalSize +
		spectrumTotalSize + sh.nbFacet * sizeof(FacetHitBuffer) + sizeof(GlobalHitBuffer);
}

void Simulation::ResetTmpCounters() {
	//SetState(0, "Resetting local cache...", false, true);

	memset(&tmpGlobalResult, 0, sizeof(GlobalHitBuffer));

	currentParticle.distTraveledSinceUpdate = 0.0;
	nbLeakSinceUpdate = 0;
	tmpGlobalResult.hitCacheSize = 0;
	tmpGlobalResult.leakCacheSize = 0;

	for (int j = 0; j < sh.nbSuper; j++) {
		for (auto& f : structures[j].facets) {

			f.ResetCounter();
			f.isHit = false;

			std::vector<TextureCell>(f.texture.size()).swap(f.texture);
			std::vector<ProfileSlice>(f.profile.size()).swap(f.profile);
			//if (f.sh.recordSpectrum)
			f.spectrum.ResetCounts();
			std::vector<DirectionCell>(f.direction.size()).swap(f.direction);
		}
	}
}

void Simulation::ResetSimulation() {
    currentParticle.lastHitFacet = NULL;
	totalDesorbed = 0;
	ResetTmpCounters();
	tmpParticleLog.clear();
}

bool Simulation::StartSimulation() {
    if (!currentParticle.lastHitFacet) StartFromSource();
    return (currentParticle.lastHitFacet != nullptr);
}

void Simulation::RecordHit(const int &type) {
	if (regions[sourceRegionId].params.showPhotons) {
		if (tmpGlobalResult.hitCacheSize < HITCACHESIZE) {
			tmpGlobalResult.hitCache[tmpGlobalResult.hitCacheSize].pos = currentParticle.position;
			tmpGlobalResult.hitCache[tmpGlobalResult.hitCacheSize].type = type;
			tmpGlobalResult.hitCache[tmpGlobalResult.hitCacheSize].dF = currentParticle.dF;
			tmpGlobalResult.hitCache[tmpGlobalResult.hitCacheSize].dP = currentParticle.dP;

			tmpGlobalResult.hitCacheSize++;
		}
	}
}

void Simulation::RecordLeakPos() {
	// Source region check performed when calling this routine 
	// Record leak for debugging
    RecordHit(HIT_REF);
    RecordHit(HIT_LAST);
	if (tmpGlobalResult.leakCacheSize < LEAKCACHESIZE) {
		tmpGlobalResult.leakCache[tmpGlobalResult.leakCacheSize].pos = currentParticle.position;
		tmpGlobalResult.leakCache[tmpGlobalResult.leakCacheSize].dir = currentParticle.direction;
		tmpGlobalResult.leakCacheSize++;
	}
}

/*bool SimulationRun() {

	// 1s step
	double t0, t1;
	size_t    nbStep = 1;
	bool   goOn;

	if (stepPerSec <= 0.0) {
		nbStep = 250;
	}
    else {
        nbStep = std::ceil(stepPerSec + 0.5);
    }

	t0 = GetTick();
	goOn = SimulationMCStep(nbStep);
	t1 = GetTick();

    if(goOn) // don't update on end, this will give a false ratio (SimMCStep could return actual steps instead of plain "false"
        stepPerSec = (1.0 * nbStep) / (t1 - t0); // every 1.0 second
#ifdef _DEBUG
	printf("Running: stepPerSec = %lf\n", stepPerSec);
#endif

	return !goOn;

}*/

/*
bool SubprocessFacet::InitializeOnLoad(const size_t &id, std::vector<Region_mathonly> &regions) {
	globalId = id;
	if (!InitializeLinkAndVolatile(id)) return false;
	if (!InitializeTexture()) return false;
	if (!InitializeProfile()) return false;
	if (!InitializeDirectionTexture()) return false;
	if (!InitializeSpectrum(regions)) return false;

	return true;
}

bool SubprocessFacet::InitializeProfile() {
	//Profiles
	if (sh.isProfile) {
		profileSize = PROFILE_SIZE * sizeof(ProfileSlice);
	try {
		profile = std::vector<ProfileSlice>(PROFILE_SIZE);
	}
	catch (...) {
		SetErrorSub("Not enough memory to load profiles");
		return false;
	}
		profTotalSize += profileSize * (1);
	}
	else profileSize = 0;
	return true;
}

bool SubprocessFacet::InitializeTexture(){
    //Textures
    if (sh.isTextured) {
        size_t nbE = sh.texWidth*sh.texHeight;
        textureSize = nbE*sizeof(TextureCell);
        try {
            texture.resize(nbE);
            textureCellIncrements.resize(nbE);
            largeEnough.resize(nbE);
        }
        catch (...) {
            SetErrorSub("Not enough memory to load textures");
            return false;
        }


        fullSizeInc = 1E30;

        for (size_t j = 0; j < nbE; j++) {
            if ((textureCellIncrements[j] > 0.0) && (textureCellIncrements[j] < fullSizeInc)) fullSizeInc = textureCellIncrements[j];
        }
        for (size_t j = 0; j < nbE; j++) { //second pass, filter out very small cells
            largeEnough[j] = (textureCellIncrements[j] < ((5.0f)*fullSizeInc));
        }
        textTotalSize += textureSize;
        iw = 1.0 / (double)sh.texWidth_precise;
        ih = 1.0 / (double)sh.texHeight_precise;
        rw = sh.U.Norme() * iw;
        rh = sh.V.Norme() * ih;
    }
    else textureSize = 0;
    return true;
}

bool SubprocessFacet::InitializeDirectionTexture(){
    //Direction
    if (sh.countDirection) {
        directionSize = sh.texWidth*sh.texHeight * sizeof(DirectionCell);
        try {
            direction = std::vector<DirectionCell>(directionSize);
        }
        catch (...) {
            SetErrorSub("Not enough memory to load direction textures");
            return false;
        }
        dirTotalSize += directionSize;
    }
    else directionSize = 0;
    return true;
}

bool SubprocessFacet::InitializeSpectrum(){
    //Spectrum
    if (sh.recordSpectrum) {
        spectrumSize = sizeof(ProfileSlice)*SPECTRUM_SIZE;
        double min_energy, max_energy;
        if (wp.nbRegion > 0) {
            min_energy = regions[0].params.energy_low_eV;
            max_energy = regions[0].params.energy_hi_eV;
        }
        else {
            min_energy = 10.0;
            max_energy = 1000000.0;
        }

        try {
            spectrum = Histogram(min_energy, max_energy, SPECTRUM_SIZE, true);
            //spectrum = std::vector<ProfileSlice>(SPECTRUM_SIZE);
        }
        catch (...) {
            SetErrorSub("Not enough memory to load spectrum");
            return false;
        }
        spectrumTotalSize += spectrumSize;
    }
    else spectrumSize = 0;

    return true;
}

bool SubprocessFacet::InitializeLinkAndVolatile(const size_t & id){
	hasVolatile |= sh.isVolatile;

	if (sh.superDest || sh.isVolatile) {
		// Link or volatile facet, overides facet settings
		// Must be full opaque and 0 sticking
		// (see SimulationMC.c::PerformBounce)
		//sh.isOpaque = true;
		sh.opacity = 1.0;
		sh.sticking = 0.0;
		if (((sh.superDest - 1) >= sh.nbSuper || sh.superDest < 0)) {
			// Geometry error
			ClearSimulation();
			//ReleaseDataport(loader);
			std::ostringstream err;
			err << "Invalid structure (wrong link on F#" << id + 1 << ")";
			SetErrorSub(err.str().c_str());
			return false;
		}
	}
	return true;
}
*/
