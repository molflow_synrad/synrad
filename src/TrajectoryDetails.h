
#ifndef _TRAJECTORYDETAILSH_
#define _TRAJECTORYDETAILSH_

#include "GLApp/GLWindow.h"
#include "GLApp/GLList.h"
#include "GLApp/GLButton.h"
#include "GLApp/GLToggle.h"
#include "GLApp/GLTitledPanel.h"
#include "GLApp/GLCombo.h"
#include "Worker.h"
#include "Random.h"

#define NB_TPCOLUMN 57

class Facet;

class TrajectoryDetails : public GLWindow {

public:

  // Construction
  TrajectoryDetails();

  // Component method
  void Display(Worker *w,int regionId);
  void Update();

  // Implementation
  void ProcessMessage(GLComponent *src,int message);
  void SetBounds(int x,int y,int w,int h);
  void SelectPoint(int idx);
  size_t GetRegionId();

private:

  char *GetCountStr(Facet *f);
  void UpdateTable();
  char *FormatCell(int idx,int mode,GenPhoton* photon);
  void PlaceComponents();

  size_t displayedRegion;
  int freq;
    MersenneTwister randomGenerator; //TODO: Couple with Interface
    Worker      *worker;
  GLList      *pointList;
  GLCombo     *regionCombo;
  GLTextField *freqText;
  GLLabel     *everyLabel;
  GLLabel     *nbPointLabel;

  GLTitledPanel *sPanel;          // Toggle panel
  GLToggle      *show[NB_TPCOLUMN-1];
  int            shown[NB_TPCOLUMN];

  GLButton    *checkAllButton;
  GLButton    *uncheckAllButton;
  GLButton    *dismissButton;
  GLButton	  *updateButton;

};

#endif /* _TRAJECTORYDETAILSH_ */
