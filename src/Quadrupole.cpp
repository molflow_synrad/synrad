
#include "Region_mathonly.h"
#include "Random.h"
#include <ctime>
#include <vector>
#include <string>
using namespace std;

Vector3d Quadrupole::B(const Vector3d &position, const double& dL_cm) {
	if (type == regular || type == combined) {

		double dX, dY, dZ; //offsets from quadrupole center
		double Bx_, By_, Bz_; //magnetic field in the transformed coordinates

		//Calculate offset
		dX = position.x - center.x;
		dY = position.y - center.y;
		dZ = position.z - center.z;

		//Transform 
		double xp = dX * cosbeta_q + dZ * sinbeta_q;
		double yp = -dX * sinalfa_q * sinbeta_q + dY * cosalfa_q + dZ * sinalfa_q * cosbeta_q;
		double zp = -dX * cosalfa_q * sinbeta_q - dY * sinalfa_q + dZ * cosalfa_q * cosbeta_q;

		dX = xp * cosrot_q + yp * sinrot_q;
		dY = -xp * sinrot_q + yp * cosrot_q;
		xp = dX;
		yp = dY;

		if (zp >= 0.0 && zp <= L_q) { //if inside the quadrupole
			Bx_ = -K_q * yp;
			By_ = -K_q * xp;
		}
		else {
			Bx_ = 0.0;
			By_ = 0.0;
		}
		Bz_ = 0.0;

		// add offset for the combined function (0 in the case of a regular quadrupole), expressed in local coordinates relative to quad axis
		if (type == combined) {
			Bx_ += magnetic_offset.x;
			By_ += magnetic_offset.y;
			Bz_ += magnetic_offset.z;
		}

		//Inverse transformation
		xp = Bx_ * cosrot_q - By_ * sinrot_q;
		yp = Bx_ * sinrot_q + By_ * cosrot_q;
		zp = Bz_;

		return Vector3d(xp * cosbeta_q - yp * sinalfa_q * sinbeta_q - zp * cosalfa_q * sinbeta_q,
			yp * cosalfa_q - zp * sinalfa_q,
			xp * sinbeta_q + yp * sinalfa_q * cosbeta_q + zp * cosalfa_q * cosbeta_q);
			
	}
	else { //Curved quadrupole
		//Project point to beding plane
		Vector3d projection = ProjectToBendingPlane(position);
		double bendAngle = GetBendAngle(projection);
		double radius = curve_rho_at_entrance.Norme();
		if (bendAngle > L_q / radius) {
			return Vector3d(0.0, 0.0, 0.0); //Outside of quadrupole
		}
		else {
			Vector3d X_prime = GetLocalRho(bendAngle).Normalized(); //Pointing towards center of curvature
			Vector3d Z_prime = GetLocalTangent(bendAngle);
			Vector3d Y_prime = CrossProduct(Z_prime, X_prime); //Right-handed coord sys
			
			//Apply quad rotation
			if (std::abs(rot_q) > 0.0) {
				X_prime = Rotate(X_prime, Vector3d(0.0, 0.0, 0.0), Z_prime, rot_q);
				Y_prime = Rotate(Y_prime, Vector3d(0.0, 0.0, 0.0), Z_prime, rot_q);
			}

			//Get local coordinates
			Vector3d localCurvePoint = GetCurvePosition(bendAngle);
			Vector3d offset = position - localCurvePoint;
			double dX = Dot(offset, X_prime);
			double dY = Dot(offset, Y_prime);

			//Apply quad field
			Vector2d B_local(K_q*dY,K_q*dX);

			//Transform back to global coordinates
			Vector3d B_global = B_local.u * X_prime + B_local.v * Y_prime;

			//Add dipole component
			B_global += magnetic_offset;

			return B_global;
		}
	}
}

Vector3d Quadrupole::ProjectToBendingPlane(const Vector3d& position)
{
	//Projection of 3D point to a plane described by a point and the normal
	//https://stackoverflow.com/questions/9605556/how-to-project-a-point-onto-a-plane-in-3d
	Vector3d v = position - curve_center;
	double dist = Dot(v, curve_rotation_axis);
	Vector3d projected = position - dist * curve_rotation_axis;
	return projected;
}

double Quadrupole::GetBendAngle(const Vector3d& position)
{
	//As per https://www.mathsisfun.com/algebra/trig-solving-sss-triangles.html
	//General case for an A,B,C triangle: cos(beta) = (c^2 + a^2 - b^2) / (2ca)
	//In our case a=(point to center), b=(point to entrance), c=radius
	double a = (position - curve_center).Norme();
	double b = (position - center).Norme(); //Center is the quad's entrance
	double c = curve_rho_at_entrance.Norme(); //Radius of curvature
	return acos((c * c + a * a - b * b) / (2 * c * a));
}

Vector3d Quadrupole::GetCurvePosition(const double& bendAngle)
{
	return Rotate(center, curve_center, curve_rotation_axis, bendAngle);
}

Vector3d Quadrupole::GetLocalTangent(const double& bendAngle)
{
	return Rotate(direction, Vector3d(0.0,0.0,0.0), curve_rotation_axis, bendAngle);
}

Vector3d Quadrupole::GetLocalRho(const double& bendAngle)
{
	return Rotate(curve_rho_at_entrance, Vector3d(0.0, 0.0, 0.0), curve_rotation_axis, bendAngle);
}
