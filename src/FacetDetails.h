
#include "GLApp/GLWindow.h"
#include "GLApp/GLList.h"
#include "GLApp/GLButton.h"
#include "GLApp/GLToggle.h"
#include "GLApp/GLTitledPanel.h"
#include "Worker.h"

#ifndef _FACETDETAILSH_
#define _FACETDETAILSH_

#define NB_FDCOLUMN 23

class Facet;

class FacetDetails : public GLWindow {

public:

  // Construction
  FacetDetails();

  // Component method
  void Display(Worker *w);
  void Update();

  // Implementation
  void ProcessMessage(GLComponent *src,int message);
  void SetBounds(int x,int y,int w,int h);

private:

  char *GetCountStr(Facet *f);
  void UpdateTable();
  char *FormatCell(size_t idx,Facet *f, size_t mode);
  void PlaceComponents();

  Worker      *worker;
  GLList      *facetListD;

  GLTitledPanel *sPanel;          // Toggle panel
  GLToggle      *show[NB_FDCOLUMN];
  int            shown[NB_FDCOLUMN];

  GLButton    *checkAllButton;
  GLButton    *uncheckAllButton;
  GLButton    *dismissButton;
  GLButton	  *updateButton;

};

#endif /* _FACETDETAILSH_ */
