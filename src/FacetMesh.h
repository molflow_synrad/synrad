
#include "GLApp/GLWindow.h"
#include "GLApp/GLToggle.h"
#include "GLApp/GLButton.h"
#include "GLApp/GLTextField.h"
#include "GLApp/GLTitledPanel.h"
#include "GLApp/GLProgress.h"
#include "Worker.h"

#ifndef _FACETMESHH_
#define _FACETMESHH_

class FacetMesh : public GLWindow {

public:

  // Construction
  FacetMesh(Worker *w);

  // Component method
  void EditFacet(std::vector<size_t> selection);

  // Implementation
  void ProcessMessage(GLComponent *src,int message);

private:

  bool Apply(bool force = false);
  void UpdateSize();
  void UpdateSizeForRatio();
    std::pair<double,double> GetRatioForNbCell(size_t nbCellsU, size_t nbCellsV);
    void UpdateSquaredCells(int aspectState);
  void UpdateToggle(GLComponent *src);
  void ApplyDrawSettings(); //Apply View Settings without stopping the simulation

  Worker   *worker;
  Geometry *geom;
  int       fIdx;

    GLTitledPanel	*aPanel;
    GLTitledPanel *iPanel;
  GLTitledPanel *vPanel;
  GLTextField   *uLength;
  GLTextField   *vLength;

  GLToggle      *enableBtn;
    GLToggle	*aspectRatioBtn;

    GLToggle      *boundaryBtn;
  GLToggle      *recordAbsBtn;
  GLToggle      *recordReflBtn;
  GLToggle      *recordTransBtn;
  GLToggle      *recordDirBtn;
  GLToggle      *showTexture;
  GLToggle      *showVolume;

    GLLabel	*l5;
    GLTextField	*lengthText;
    GLTextField	*lengthText2;
    GLLabel* labelXLen;
    GLLabel* labelXRes;
    GLLabel	*perCm;
    GLLabel	*perCell;
    GLTextField   *resolutionText;
    GLTextField	*resolutionText2;

    // NxM texture input
    GLLabel* nbCell;
    GLTextField* cellsU;
    GLLabel* labelXCells;
    GLTextField* cellsV;

  GLTextField   *ramText;
  GLTextField   *cellText;
  GLButton      *updateButton;

  GLButton    *applyButton;
  GLButton	  *quickApply; //Apply View Settings without stopping the simulation
  GLButton    *cancelButton;

  GLProgress  *progressDlg;

};

#endif /* _FACETMESHH_ */
