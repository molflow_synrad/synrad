
#include <filesystem>
#include <Helper/StringHelper.h>
#include "RegionSelection.h"
#include "GeneratePhoton.h"
#include "GLApp/GLToolkit.h"
#include "Helper/MathTools.h" //Pi Min max
#include "GLApp/GLMessageBox.h"
#include "SynradDistributions.h"

#include "SynRad.h"
#include "Random.h"

extern SynRad *mApp;

typedef struct {

	const char *name;
	const int   width;
	const int   align;

} COLUMN;

static COLUMN allColumn[] = {
	{ "#", 40, ALIGN_CENTER },
    { "nb Points", 80, ALIGN_CENTER },
	{ "Filename", 300, ALIGN_CENTER }
};

RegionSelection::RegionSelection(Worker *worker) : GLWindow() {

	int wD = 30; // left,right buffer
	for (auto&col : allColumn){
        wD += col.width;
	}
	int hD = 200;
	this->worker = worker;

    SetTitle("Region View selector");
	SetIconfiable(true);
	SetResizable(true);
	SetMinimumSize(200, 300);

	updateButton = new GLButton(0, "Update table!");
	Add(updateButton);
    updateSelButton = new GLButton(0, "Update selection!");
    Add(updateSelButton);
	dismissButton = new GLButton(0, "Dismiss");
	Add(dismissButton);

    selectButton = new GLButton(0, "Select");
    Add(selectButton);
    deselectButton = new GLButton(0, "Deselect");
    Add(deselectButton);
    selectionText = new GLTextField(0, "");
    selectionText->SetEditable(true);
    Add(selectionText);

    regionList = new GLList(0);
    regionList->SetSelectionMode(MULTIPLE_ROW);
    regionList->SetSelectionMode(MULTIPLE_ROW);
    regionList->SetColumnLabelVisible(true);
    regionList->SetGrid(true);
	Add(regionList);

    selectedRegions = mApp->selectedRegions;
    UpdateSelection(true);

	sPanel = new GLTitledPanel("Show column");
	sPanel->SetClosable(true);
	sPanel->Close();
	Add(sPanel);

	for (int i = 1; i < NB_RPCOLUMN; i++) {
		show[i - 1] = new GLToggle(0, allColumn[i].name);
		show[i - 1]->SetState(true);
		sPanel->Add(show[i - 1]);
	}

	checkAllButton = new GLButton(0, "Check All");
	sPanel->Add(checkAllButton);
	uncheckAllButton = new GLButton(0, "Uncheck All");
	sPanel->Add(uncheckAllButton);

	// Center dialog
	int wS, hS;
	GLToolkit::GetScreenSize(&wS, &hS);
	int xD = (wS - wD) / 2;
	int yD = (hS - hD) / 2;
	SetBounds(xD, yD, wD, hD);

	RestoreDeviceObjects();
    UpdateTable();
}

void RegionSelection::PlaceComponents() {

	// Show toggle panel
	int nbW = (width - 20) / 120;
	int nbL = (NB_RPCOLUMN - 1) / nbW + (((NB_RPCOLUMN - 1) % nbW) ? 1 : 0);
	int hp;
	if (!sPanel->IsClosed())
		hp = 20 * (nbL + 1) + 25;
	else
		hp = 20;
	sPanel->SetBounds(5, height - (hp + 76), width - 10, hp);
	for (int i = 0; i < (NB_RPCOLUMN - 1); i++)
		sPanel->SetCompBounds(show[i], 5 + 120 * ((i) % nbW), 18 + 20 * ((i) / nbW), 125, 19);

	regionList->SetBounds(5, 5, width - 10, height - (82 + hp));

	sPanel->SetCompBounds(uncheckAllButton, width / 2 - 90, hp - 25, 90, 19);
	sPanel->SetCompBounds(checkAllButton, width / 2 + 5, hp - 25, 90, 19);

    selectionText->SetBounds(5, height - 69, 100, 19);
    selectButton->SetBounds(110, height - 69, 80, 19);
    deselectButton->SetBounds(195, height - 69, 80, 19);

	updateButton->SetBounds(width - 420, height - 45, 155, 19);
    updateSelButton->SetBounds(width - 260, height - 45, 155, 19);
	dismissButton->SetBounds(width - 100, height - 45, 90, 19);
}

void RegionSelection::SetBounds(int x, int y, int w, int h) {

	GLWindow::SetBounds(x, y, w, h);
	PlaceComponents();

}

char *RegionSelection::FormatCell(int idx, int mode, Region_full *reg) {

	static char ret[256];
	strcpy(ret, "");

	switch (mode) {
	case 0: //Region id
		sprintf(ret, "%d", idx + 1);
		break;
	case 1: //Number of Points in Region
        sprintf(ret, "%d", reg->Points.size());
        break;
	case 2: //Filename
    {
        std::string fileName;
        if (!reg->fileName.empty())
            fileName = std::filesystem::path(reg->fileName).filename().string();
        sprintf(ret, "%s", fileName.c_str());
        break;
    }
	default:
		sprintf(ret, "%s", "");
		break;
	}

	return ret;

}

void RegionSelection::UpdateTable() {

	GLProgress *updatePrg = new GLProgress("Updating table...", "Please wait");
	updatePrg->SetProgress(0.0);
	updatePrg->SetVisible(true);

	//initialize random number generator

	static char ret[256];
	strcpy(ret, "");

	const char *tmpName[NB_RPCOLUMN];
	int  tmpWidth[NB_RPCOLUMN];
	int  tmpAlign[NB_RPCOLUMN];

	int  nbCol = 0;

	for (int i = 0; i < NB_RPCOLUMN; i++) {
		if (i == 0 || show[i - 1]->GetState()) {
			tmpName[nbCol] = allColumn[i].name;
			tmpWidth[nbCol] = allColumn[i].width;
			tmpAlign[nbCol] = allColumn[i].align;
			shown[nbCol] = i;
			nbCol++;
		}
	}

	int nbRegions = (int)worker->regions.size();

    regionList->SetSize(nbCol, (nbRegions));
    regionList->SetColumnWidths(tmpWidth);
    regionList->SetColumnLabels(tmpName);
    regionList->SetColumnAligns(tmpAlign);

    for (int regionId = 0; regionId < nbRegions; ++regionId) {
        updatePrg->SetProgress((double)regionId / (double)nbRegions);
        for (int j = 0; j < nbCol; j++)
            regionList->SetValueAt(j, regionId, FormatCell(regionId, shown[j], &worker->regions[regionId]));
    }

    updatePrg->SetVisible(false);
	SAFE_DELETE(updatePrg);
}

void RegionSelection::Update() {

	if (!worker) return;
	if (!IsVisible()) return;

    UpdateSelection(false);
	UpdateTable();

}

void RegionSelection::UpdateSelection(bool externalChange) {
    if(externalChange){
        tmpSelectedRegions.clear();
        tmpSelectedRegions.insert(tmpSelectedRegions.begin(), selectedRegions->begin(), selectedRegions->end());
    }
    else{
        selectedRegions->clear();
        selectedRegions->insert(tmpSelectedRegions.begin(), tmpSelectedRegions.end());
    }
    regionList->ClearSelection();
    regionList->SetSelectedRows(tmpSelectedRegions);
}

void RegionSelection::Display(Worker *w, int regionId) {

	worker = w;
	displayedRegion = regionId;

	SetVisible(true);
	Update();

}

void RegionSelection::ProcessMessage(GLComponent *src, int message) {

	switch (message) {

	case MSG_BUTTON:
		if (src == dismissButton) {
			SetVisible(false);
		}
		else if (src == checkAllButton) {
			for (int i = 0; i < NB_RPCOLUMN - 1; i++) show[i]->SetState(true);
			//UpdateTable();
		}
		else if (src == uncheckAllButton) {
			for (int i = 0; i < NB_RPCOLUMN - 1; i++) show[i]->SetState(false);
			//UpdateTable();
		}
		else if (src == updateButton) {
			UpdateTable();
		}
        else if (src == updateSelButton) {
            UpdateSelection(false);
            mApp->UpdateRegionSelection();
        }
        else if (src == selectButton) {
            std::vector<size_t> regionIds;
            try {
                splitList(regionIds, selectionText->GetText(), worker->regions.size());
                if(regionIds.empty())
                    break;
            }
            catch (std::exception &e) {
                GLMessageBox::Display(e.what(), "Error", GLDLG_OK, GLDLG_ICONERROR);
                break;
            }
            catch (...) {
                GLMessageBox::Display("Unknown exception", "Error", GLDLG_OK, GLDLG_ICONERROR);
                break;
            }

            std::sort( regionIds.begin(), regionIds.end() );
            regionIds.erase( std::unique( regionIds.begin(), regionIds.end() ), regionIds.end());
            tmpSelectedRegions = regionIds;

            UpdateSelection(false);
            mApp->UpdateRegionSelection();
        }
        else if (src == deselectButton) {
            std::vector<size_t> regionIds;
            try {
                splitList(regionIds, selectionText->GetText(), worker->regions.size());
                if(regionIds.empty())
                    break;
            }
            catch (std::exception &e) {
                GLMessageBox::Display(e.what(), "Error", GLDLG_OK, GLDLG_ICONERROR);
                break;
            }
            catch (...) {
                GLMessageBox::Display("Unknown exception", "Error", GLDLG_OK, GLDLG_ICONERROR);
                break;
            }
            std::sort(regionIds.begin(), regionIds.end());
            std::sort(tmpSelectedRegions.begin(), tmpSelectedRegions.end());

            bool selChanged = false;
            auto iter = tmpSelectedRegions.begin();
            for(auto id : regionIds){
                while (iter != tmpSelectedRegions.end()) {
                    if(id == *iter){
                        // id match, delete
                        iter = tmpSelectedRegions.erase(iter);
                        selChanged = true;
                    }
                    else if (id < *iter){
                        // id passed sorted value, so it's not in the list
                        break;
                    }
                    else{
                        ++iter;
                    }
                }
            }
            if(selChanged) {
                UpdateSelection(false);
                mApp->UpdateRegionSelection();
            }
        }
		break;

	case MSG_TOGGLE:
		//UpdateTable();
		break;

		/*case MSG_LIST_COL:
			if( src==facetListD ) {
			// Save column width
			int c = facetListD->GetDraggedCol();
			allColumn[shown[c]].width = facetListD->GetColWidth(c);
			}
			break;*/

	case MSG_PANELR:
		PlaceComponents();
		break;

	case MSG_LIST: {
        auto selRows = regionList->GetSelectedRows(true);
        tmpSelectedRegions.clear();
        for (auto &sel : selRows) {
            tmpSelectedRegions.push_back(sel);
        }
        break;
    }
	case MSG_TEXT_UPD:{
		break;
	}
	}

	GLWindow::ProcessMessage(src, message);
}

size_t RegionSelection::GetRegionId() {
	return displayedRegion;
}

void RegionSelection::SelectPoint(int idx) {
    regionList->SetSelectedRow((int)((double)idx));
    regionList->ScrollToVisible((int)((double)idx), 0, false);
}