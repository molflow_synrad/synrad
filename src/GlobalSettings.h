
#ifndef _GLOBALSETTINGSH_
#define _GLOBALSETTINGSH_

#include "GLApp/GLWindow.h"
#include "GLApp/GLButton.h"
#include "GLApp/GLTextField.h"
#include "GLApp/GLLabel.h"
#include "GLApp/GLToggle.h"
#include "GLApp/GLTitledPanel.h"
#include "GLApp/GLGradient.h"
#include "Buffer_shared.h" //MAX_PROCESS macro

class Worker;
class GLList;

class GlobalSettings : public GLWindow {

public:

  // Construction
  GlobalSettings(Worker *w);
  void SMPUpdate();

  // Implementation
  void ProcessMessage(GLComponent *src,int message);
  void Display(Worker *w);
    void Update();

private:

	void RestartProc();
	  Worker      *worker;
  GLList      *processList;
  GLButton    *restartButton;
  GLButton    *maxButton;
  GLTextField *nbProcText;
  GLTextField *autoSaveText;
  GLTextField *cutoffText;
 
  int lastUpdate;
  //float lastCPUTime[MAX_PROCESS];
  //float lastCPULoad[MAX_PROCESS];

  GLToggle      *chkAntiAliasing;
  GLToggle      *chkWhiteBg;
  GLToggle		*leftHandedToggle;
  GLToggle* highlightNonplanarToggle;
  GLToggle      *chkSimuOnly;
  GLToggle      *chkCheckForUpdates;
  GLToggle      *chkAutoUpdateFormulas;
  GLToggle      *chkNewReflectionModel;
  GLToggle      *chkCompressSavedFiles;
  GLToggle      *lowFluxToggle;
  GLButton    *applyButton;
  GLButton    *cancelButton;
  GLButton    *lowFluxInfo;
  GLButton    *newReflectmodeInfo;

  /*GLTextField *outgassingText;
  GLTextField *gasmassText;*/
  GLToggle *highlightSelectionToggle;

};

#endif /* _GLOBALSETTINGSH_ */
