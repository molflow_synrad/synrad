//
// Created by Pascal Baehr on 28.04.20.
//

#include <sstream>
#include <ProcessControl.h>
#include "GeometrySimu.h"
#include "Region_mathonly.h"

void ClearSimulation();

SuperStructure::SuperStructure()
{
    aabbTree = NULL;
}

SuperStructure::~SuperStructure()
{
    SAFE_DELETE(aabbTree);
}


bool SubprocessFacet::InitializeLinkAndVolatile(const size_t & id)
{
    if (sh.superDest || sh.isVolatile) {
        // Link or volatile facet, overides facet settings
        // Must be full opaque and 0 sticking
        // (see SimulationMC.c::PerformBounce)
        //sh.isOpaque = true;
        sh.opacity = 1.0;
        sh.sticking = 0.0;
    }
    return true;
}

void SubprocessFacet::ResetCounter() {
    memset(&tmpCounter, 0, sizeof(tmpCounter));
    tmpCounter.ResetBuffer();
}


bool SubprocessFacet::InitializeOnLoad(const size_t &id, std::vector<Region_mathonly> &regions) {
    globalId = id;
    if (!InitializeLinkAndVolatile(id)) return false;
    if (!InitializeTexture()) return false;
    if (!InitializeProfile()) return false;
    if (!InitializeDirectionTexture()) return false;
    if (!InitializeSpectrum(regions)) return false;

    return true;
}

bool SubprocessFacet::InitializeProfile() {
    //Profiles
    if (sh.isProfile) {
        profileSize = PROFILE_SIZE * sizeof(ProfileSlice);
        try {
            profile = std::vector<ProfileSlice>(PROFILE_SIZE);
        }
        catch (...) {
            throw std::runtime_error("Not enough memory to load profiles");
            return false;
        }
    }
    else profileSize = 0;
    return true;
}

bool SubprocessFacet::InitializeTexture(){
    //Textures
    if (sh.isTextured) {
        size_t nbE = sh.texWidth*sh.texHeight;
        textureSize = nbE*sizeof(TextureCell);
        try {
            texture.resize(nbE);
            textureCellIncrements.resize(nbE);
            largeEnough.resize(nbE);
        }
        catch (...) {
            throw std::runtime_error("Not enough memory to load textures");
            return false;
        }


        fullSizeInc = 1E30;

        for (size_t j = 0; j < nbE; j++) {
            if ((textureCellIncrements[j] > 0.0) && (textureCellIncrements[j] < fullSizeInc)) fullSizeInc = textureCellIncrements[j];
        }
        for (size_t j = 0; j < nbE; j++) { //second pass, filter out very small cells
            largeEnough[j] = (textureCellIncrements[j] < ((5.0f)*fullSizeInc));
        }
        iw = 1.0 / (double)sh.texWidth_precise;
        ih = 1.0 / (double)sh.texHeight_precise;
        rw = sh.U.Norme() * iw;
        rh = sh.V.Norme() * ih;
    }
    else textureSize = 0;
    return true;
}

bool SubprocessFacet::InitializeDirectionTexture(){
    //Direction
    if (sh.countDirection) {
        directionSize = sh.texWidth*sh.texHeight * sizeof(DirectionCell);
        try {
            direction = std::vector<DirectionCell>(directionSize);
        }
        catch (...) {
            throw std::runtime_error("Not enough memory to load direction textures");
            return false;
        }
    }
    else directionSize = 0;
    return true;
}

bool SubprocessFacet::InitializeSpectrum(std::vector<Region_mathonly>& regions){
    //Spectrum
    if (sh.recordSpectrum) {
        spectrumSize = sizeof(ProfileSlice)*SPECTRUM_SIZE;
        double min_energy, max_energy;
        if (!regions.empty()) {
            min_energy = regions[0].params.energy_low_eV;
            max_energy = regions[0].params.energy_hi_eV;
        }
        else {
            min_energy = 10.0;
            max_energy = 1000000.0;
        }

        try {
            spectrum = Histogram(min_energy, max_energy, SPECTRUM_SIZE, true);
            //spectrum = std::vector<ProfileSlice>(SPECTRUM_SIZE);
        }
        catch (...) {
            throw std::runtime_error("Not enough memory to load spectrum");
            return false;
        }
    }
    else spectrumSize = 0;

    return true;
}