#pragma once
#include <string>

//Hard-coded identifiers, update these on new release and rebuild solution
//---------------------------------------------------
static const std::string appName = "Synrad";
static const int appVersionId = 1436; //Compared with available updates. Global variable, so rebuild whole solution if changed.
static const std::string appVersionName = "1.4.36";
//---------------------------------------------------
#ifdef _DEBUG
static const std::string appTitle = "Synrad+ debug version (Compiled " __DATE__ " " __TIME__ ")";
#else
static const std::string appTitle = "Synrad+ " + appVersionName + " (" __DATE__ ")";
#endif