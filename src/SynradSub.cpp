

#if defined(WIN32) || defined(_WIN32) || defined(WIN64) || defined(_WIN64)
#define NOMINMAX
#include <Windows.h>
#include <TlHelp32.h>
#include <process.h> // For _getpid()
#endif

//#include <iostream>

#include <stdio.h>
#include <math.h>
#include <time.h>
#include <cstring>
#include <SimulationController.h>
#include "Buffer_shared.h"
#include "Simulation.h"
#include "ProcessControl.h"

// Global process variables

#define WAITTIME    100  // Answer in STOP mode

int main(int argc,char* argv[])
{

    if(argc!=3) {
        printf("Usage: synradSub peerId index\n");
        return 1;
    }

    size_t hostProcessId = atoi(argv[1]);
    size_t prIdx = atoi(argv[2]);
    SimulationController simController = {"synrad", "SNRD", hostProcessId, prIdx, new Simulation()};

    InitTick();

    // Main loop
    simController.controlledLoop();

    return 0;

}
