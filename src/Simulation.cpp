
#include "Simulation.h"
#include "IntersectAABB_shared.h"
#include <cstring>

Simulation::Simulation()
{
	memset(&tmpGlobalResult, 0, sizeof(GlobalHitBuffer));
	
    nbLeakSinceUpdate = 0;

    totalDesorbed = 0;

    currentParticle.lastHitFacet = nullptr;
    currentParticle.structureId = 0;
    currentParticle.teleportedFrom = 0;

    sh.nbSuper = 0;

    // Geometry
    nbMaterials = 0;
    sourceArea = 0;
    nbDistrPoints_BXY = 0;
    sourceRegionId = 0;

    stepPerSec = 0.0;
    textTotalSize = 0;
    profTotalSize = 0;
    dirTotalSize = 0;
    spectrumTotalSize = 0;
    loadOK = false;
    lastHitUpdateOK = false;
    lastLogUpdateOK = false;
    hasVolatile = false;

    wp.nbRegion = 0;
    wp.nbTrajPoints = 0;
    wp.newReflectionModel = false;

    currentParticle = CurrentParticleStatus();
}

Simulation::~Simulation(){

}

void Simulation::ClearSimulation() {
    memset(&tmpGlobalResult, 0, sizeof(GlobalHitBuffer));

    nbLeakSinceUpdate = 0;

    totalDesorbed = 0;

    currentParticle.lastHitFacet = nullptr;
    currentParticle.structureId = 0;
    currentParticle.teleportedFrom = 0;

    sh.nbSuper = 0;

    // Geometry
    nbMaterials = 0;
    sourceArea = 0;
    nbDistrPoints_BXY = 0;
    sourceRegionId = 0;

    stepPerSec = 0.0;
    textTotalSize = 0;
    profTotalSize = 0;
    dirTotalSize = 0;
    spectrumTotalSize = 0;
    loadOK = false;
    lastHitUpdateOK = false;
    lastLogUpdateOK = false;
    hasVolatile = false;

    wp.nbRegion = 0;
    wp.nbTrajPoints = 0;
    wp.newReflectionModel = false;

    this->currentParticle = CurrentParticleStatus();
    this->vertices3.clear();
    this->structures.clear();
    this->chi_distros.clear();
    this->materials.clear();
    this->parallel_polarization.clear();
    this->psi_distro.clear();
    this->tmpParticleLog.clear();
}

int Simulation::ReinitializeParticleLog() {
    tmpParticleLog.clear();
    tmpParticleLog.shrink_to_fit();
    if (ontheflyParams.enableLogging)
        tmpParticleLog.reserve(ontheflyParams.logLimit / ontheflyParams.nbProcess);

    return 0;
}