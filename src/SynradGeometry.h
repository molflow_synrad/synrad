
#pragma once

#include "Geometry_shared.h"
#include "Region_full.h"
//#include <cereal/archives/json.hpp>
#include <sstream>
#include <pugixml.hpp>

#define SYNVERSION   12
//12: added newReflectionModel, lowFluxMode, lowFluxCutoff (previously app settings)

#define PARAMVERSION 5
//4: added structureId
//5: added globalComponents, startS

class Worker;
class Material;

class SynradGeometry: public Geometry {

#pragma region Geometry.cpp
private:
	//void CalculateFacetParam(int facetId); // Facet parameters
	std::vector<std::string> InsertSYNGeom(FileReader *file, size_t strIdx, bool newStruct = false);
	void SaveProfileSYN(FileWriter *file, BYTE *buffer, int super = -1, bool saveSelected = false, bool crashSave = false);
	void SaveSpectrumSYN(FileWriter *file, BYTE *buffer, int super = -1, bool saveSelected = false, bool crashSave = false);
	//void SaveProfileGEO(FileWriter *file, int super = -1, bool saveSelected = false);
public:
	SynradGeometry();
	void LoadGEO(FileReader *file, GLProgress *prg, int *version, Worker *worker);
	std::vector<std::string> LoadSYN(FileReader *file, GLProgress *prg, int *version, Worker *worker);
	bool LoadTextures(FileReader *file, GLProgress *prg, BYTE *buffer, int version);
    std::vector<std::string> InsertSYN(FileReader *file, GLProgress *prg, bool newStr);
	void SaveTXT(FileWriter *file, BYTE *buffer, bool saveSelected);
	void ExportTextures(FILE *file, int grouping, int mode, double no_scans, BYTE *buffer, bool saveSelected);
	void SaveDesorption(FILE *file, BYTE *buffer, bool selectedOnly, int mode, double eta0, double alpha, const Distribution2D &distr); //Deprecated, not used anymore

	//void SaveGEO(FileWriter *file,GLProgress *prg,Dataport *dpHit,bool saveSelected,LEAK *pleak,int *nbleakSave,HIT *hitCache,int *nbHHitSave,bool crashSave=false);
	void SaveSYN(FileWriter *file, GLProgress *prg, BYTE *buffer, bool saveSelected, LEAK *leakCacheSave, size_t *nbLeakSave, HIT *hitCacheSave, size_t *nbHitSave, bool crashSave = false);
	void SaveXML_geometry(pugi::xml_node saveDoc, Worker *work, GLProgress *prg, bool saveSelected);
	bool SaveXML_simustate(pugi::xml_node saveDoc, Worker *work, BYTE *buffer, GlobalHitBuffer *gHits, int nbLeakSave, int nbHHitSave,
		LEAK *leakCache, HIT *hitCache, GLProgress *prg, bool saveSelected);
	void LoadXML_geom(pugi::xml_node loadXML, Worker *work, GLProgress *progressDlg);
	void InsertXML(pugi::xml_node loadXML, Worker *work, GLProgress *progressDlg, bool newStr);
	bool LoadXML_simustate(pugi::xml_node loadXML, BYTE *buffer, Worker *work, GLProgress *progressDlg);
	void BuildPipe(double L, double R, double s, int step);
	void LoadProfileSYN(FileReader *file, BYTE *buffer, const int& version);
	void LoadSpectrumSYN(FileReader *file, BYTE *buffer, const int& version);
    bool LoadConvDataSYN(FileReader *file);

    size_t GetGeometrySize(std::vector<Region_full> &regions, std::vector<Material> &materials,
		std::vector<std::vector<double>> &psi_distro, std::vector<std::vector<std::vector<double>>> &chi_distros,
		const std::vector<std::vector<double>>& parallel_polarization);
	size_t GetHitsSize();
	void CopyGeometryBuffer(BYTE *buffer, std::vector<Region_full> &regions, std::vector<Material> &materials,
		std::vector<std::vector<double>> &psi_distro, const std::vector<std::vector<std::vector<double>>> &chi_distros,
		const std::vector<std::vector<double>> &parallel_polarization, const bool& newReflectionModel, const OntheflySimulationParams& ontheflyParams);
#pragma endregion

#pragma region GeometryRender.cpp
	void BuildFacetTextures(BYTE *hits, bool renderRegularTexture, bool renderDirectionTexture);
#pragma endregion

    void SerializeForLoader(cereal::BinaryOutputArchive &outputArchive);

	// Temporary variable (used by LoadXXX)
	double loaded_totalFlux;
	double loaded_totalPower;
	double loaded_no_scans;

};

